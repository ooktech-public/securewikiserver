# Configuration for the Secure Wiki Server

These are the configuration options for the secure wiki server

- `wikiPathBase` - The file server uses this as the base of relative paths for
  the file server. The value `homedir` uses the current logged in users home
  directory, the value `cwd` uses the current working directory. The paths set
  below can all either be absolute paths or, if they are relative paths, they
  are relative to the value set here.
- `serverKeyPath` - The path to the private key that goes with your https
  certificate.
- `certPath` - This is the path to the certificate file used for https.
- `tokenPrivateKeyPath` - This is the path to the private key used to sign
  by the JWT module to sign access tokens.
- `wikiPermissionsPath` - This is the path to the default wiki permissions
  file.
- `localWikiPermissionsPath` - This is the path to the local wiki permissions
  file.
- `httpsPort` - This is the port used by the https server.
- `tokenTTL` - This is the time to live value given to access tokens. When
  someone logs into the server their access token will be good for this long.
  When the time to live runs out the person will have to log in again.
- `saltRounds` - This is the number of salt rounds used by bcrypt for
  encrypting password hashes. It must be a value greater than 10.
- `serveWikiOnRoot` - If this is set to `true` than the index wiki is displayed
  on `/`, otherwise the root wiki is served on `/wiki` and other wikis are
  served below that. (ex. `/wiki/SomeWikiName`)
- `wikisPath` - This is the path on the local file system to where the wikis
  are stored.
- `pluginsPath` - This is the path to the plugins folder on the server.
- `themesPath` - This is the path to the themes folder on the server.
- `rootWikiName` - This is the name of the wiki to use as the root wiki.
- `reverseProxy` - I don't remember what this part does. Something to do with
  CORS and what is needed based on if you have a reverse proxy set up.
- `restrictPushFetch` - If this is set to `yes` than a wiki owner doesn't
  automatically get permissions to push and fetch to/from their wiki.

### Actions Section

- `actions` - This section sets specifics about what different permissions
  allow. Unless you know what you are doing and want to change some basic
  behaviors of the server you shouldn't modify these.
  Each value is an array of the names of websocket messages that are allowed
  for someone who has that permission. People can have multiple permissions
  listed for a wiki so `edit` doesn't necessarily have to have all of the
  permissions included in `view` because a person could be given both
  permissions.
  - `view` - A list of messages required to be able to view a wiki with Bob's
    features like seeing changes as the happen without a reload.
  - `edit` - A list of messages required to edit a wiki.
  - `admin` - A list of messages required for administrating a wiki server.
    This permission gives access to administration functions on the server that
    affect more than just one wiki, so be careful.
  - `scripts` - A list of messages that are used to run scripts on the server
    from within a wiki. Be careful with this because scripts run on the server
    are run by the same user that is running the server which may have sudo
    permissions. This is only intended to be used on servers that are only
    accessible on a private local network, like a home network.
  - `owner` - A list of messages that are given to the person marked as the
    owner of a specific wiki. It covers `view`, `edit` and some wiki-specific
    administration messages.
  - `upload` - Nothing is listed here, this permission allows uploading files
    to the server. (Check if this is a global permission or just for the one
    wiki)
  - `push` - Nothing is listed here, this permission allows pushing tiddlers to
    a wiki.
  - `fetch` - Nothing is listed here. This permission allows fetching tiddlers
    from a wiki.

### API section

- `API` - settings in this group affect the http(s) api endpoints available on
    the server. Each of these just enable or disable endpoints, they do not set
    the permissions required to use the endpoint.
  - `enableFetch` - if this is set to `yes` the fetching endpoint is enabled.
  - `enablePush` - if this is set to `yes` the endpoint for pushing is enabled.
  - `pluginLibrary` - If this is set to `yes` the plugin library is enabled
    with all of it's endpoints. This includes the endpoints to list, fetch and
    upload plugins.
  - `updatePasswords` - If this is set to `yes` the endpoint for updating a
    persons password is enabled.
  - `fetchEndpoint` - if this is set to `yes` the endpoint for fetching
    tiddlers is enabled.
  - `pushEndpoint` - if this is set to `yes` the endpoint for pushing tiddlers
    to wikis is enabled.

### Admin section

- `admin` - This group affects what people logged in can do to the server based
  on their logged in level, which is different than the levels of access to
  individual wikis.
  - `pushPlugins` - This lists the login levels that have permission to push
    plugins to the wiki to add them to the library or update them.
  - `viewSettings` - This lists the login levels that have permission to view
    the wiki server settings.
  - `editSettings` - This lists the login levels that have permission to edit
    the wiki server settings.
