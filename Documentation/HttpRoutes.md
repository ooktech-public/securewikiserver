# HTTP routes used by the server for normal functions

Note that for all of this the wiki names can be any length with multiple levels
separated by `/`, they don't have to just be one part.
And not every part has to be a wiki, paths could be like this:

- `/AWiki`
- `/AWiki/NotAWiki/AnotherWiki`

## Accessing wikis

- GET `/` - if the server is setup to serve wikis on `/` than this route gives the
  root wiki.
- GET `/wikiName` - this gives the wiki named `wikiName` in the wiki listing.
  - GET `/wikiName/wikiName2`, GET `/wikiName/wikiName2/Wiki3` and so on give the wikis
    listed in the wiki listing in the appropriate place.
  - GET `/person/wikiName` gives the wiki `wikiName` belonging to `person`.
- GET `/favicon.ico` - the favicon for the wiki mounted on `/`
  - This goes for other wiki routes too, any wiki path has a `./favicon.ico`
    route to get the favicon.

## Accessing files

- GET `/files/:filePath` - this returns the file at `:filePath` on the server. This
  is the globally accessible endpoint, these files can be used by any wiki on
  the server.
- GET `/wikiName/files/:filePath` - this returns the file at `:filePath` on the
  server. This is for files that are only accessible to the one wiki.
  - That isn't entirely accurate, the files are accessible to anyone who has
    the `view` permission on the wiki in question.
  - The `wikiName` portion can be a longer path like `person/wiki1/wiki2` etc.

## Uploading files

- POST `/upload` - this route allow you to upload files to the server. This is
  specifically for the `OokTech/ServerImages` plugin and expects input in the
  form of a tiddler from a wiki.
  - Body Contents:
    - `wiki` - the name of the wiki the file is for
    - `tiddler` - the tiddler object that has the file to upload as a base64
      encoded string in the text field.

## Authentication

- POST `/authenticate` - this route is used to login and get an access token.
  - Body Contents:
    - `name` - the login name for the person trying to get authenticated
    - `pwd` - the password for the person trying to get authenticated
