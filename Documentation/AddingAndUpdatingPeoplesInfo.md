# Managing peoples info

There are two functions for managing login information.

They are defined in `addperson.js`.

## updateCredentials

This function is only available to administrators.
It allows you to add new people or to modify a persons credentials.

To add a new person you need to provide a name, password and level.

Afterward you can modify the persons password and level using this function.

## changePassword

This function is available to people who have accounts and is used to change
their passwords.
