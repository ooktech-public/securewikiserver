# Authentication Levels and Permissions

There are two distinct types of access control on the server.
One controls access to server administration functions and the other controls
access to the contents of the server.
For these descriptions I will call them `Server` and `Wiki` authentication
levels.

Most of the server authentication level access hasn't been implemented yet.

## Server authentication levels

When a user is created they are given a level. By default this is `Guest` which
has no special permissions and can't affect anything other than wikis.
Then there is the `Normal` level, which in terms of server access is equivalent
to `Guest` but may have more permissions for wiki authentication levels.

The `Admin` authentication level gives full access to the server.

New authentication levels can be defined, but for the moment they don't have
much use aside from being a convenient grouping for setting wiki authentication
levels.

## Wiki authentication levels and permissions

There is fine grained access control for each wiki to define who can view,
edit, pull tiddlers from or push tiddlers to that wiki.

There are multiple ways to change and set these permissions:

### Settings in the `__permissions` section

Each wiki has its own section called `__permissions` in the wiki permissions
file.
These settings go into that section are per-wiki settings.
The descriptions are the defaults, most of the permissions can be changed by
editing the wiki permissions or config files.

- The person listed as the owner can view, edit and has access to wiki-specific
  administration functions. The most important is probably `setWikiPermissions`
  which lets the owner of a wiki change the permissions for their wiki. See
  `Config.toml` for specifics.
- if in the `__permissions` section of the wiki permissions file `public` is
  set to `true` anyone can view the wiki as a single html file (it doesn't get
  live updates and doesn't communicate with the server)
- Per-person permissions can be set by adding names to the `editors`,
  `fetchers`, `viewers` and `pullers` lists. The permissions granted to people
  on each list are exactly equivalent to the `edit`, `fetch`, `view` and `pull`
  permissions.
- Per-group permissions for each wiki can be set based on the server
  authentication levels. A sub-section of the `__permissions` section for a
  wiki called `access` lists the permissions given to anyone with each
  authentication level from the server.
