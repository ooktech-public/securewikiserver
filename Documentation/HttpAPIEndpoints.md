# API endpoints the server can make available

For each of the following endpoints if anything in the POST body is needed it
will be listed under the endpoint description.
The POST body should be a stringified JSON object where the keys are the
parameter name and the values are the parameter values.

All of the api endpoints can take a `token` parameter that contains the access
token if one is necessary.

Example for `/api/fetch` (the json portion is formatted for clarity):

```
message={
  "type": "fetch",
  "token":<<ACCESS TOKEN>>,
  "filter":"[!is[system]limit[10]]",
  "fromWiki":"MyWiki",
  "fieldList":"text title tags"
}
```

## Managing people

- POST `/api/credentials/add/:userName` - POSTs to this endpoint let you add a new
  user on the server if you have the proper credentials.
  - Body contents (the username is given in the url and isn't repeated here):
    - `level` - the access level given. This is the server access level, not
      for wikis. See `AccessLevels.md` for more.
    - `password` - the password for the person
- POST `/api/credentials/update/:userName` - POSTs to this endpoint let you update
  a users password.
  - Body contents:
    - `pwd` - the current password for the person
    - `level` - the new level to set for the person (optional)
    - `new` - the new password for the person

## Getting and fetching tiddlers

- POST `/api/push` - POSTs to this endpoint allow you to push tiddlers to a specific
  wiki, if you have the correct token.
  - Body contents:
    - ``
- POST `/api/fetch` - POSTs to this endpoint allow you to fetch tiddlers from a
  specific wiki if it is either public or you have the correct access token.
  - Body contents:
    - `filter` - The filter used to determine which tiddlers to fetch from the
      remote wiki.
    - `fromWiki` - The name of the wiki to pull the tiddlers from.
    - `fieldList` - An optional list of fields to return as a space separated
      list. If this is set than only fields in the list will be included in the
      returned tiddlers.
- POST `/api/fetch/list` - POSTs to this endpoint returns a title list that shows
  tiddlers that would be returned by a filter used with the `/api/fetch`
  endpoint.
  - Body contents:
  - `filter` - The filter used to determine which tiddlers to fetch from the
    remote wiki.
  - `fromWiki` - The name of the wiki to pull the tiddlers from.
  - `fieldList` - An optional list of fields to return as a space separated
    list. If this is set the fields listed are returned in addition to the
    title.

## Managing plugins

- POST `/api/plugins/list` - POSTs to this endpoint get a response that contains a
  list of plugins available on the server along with some metadata for the
  plugins.
- POST `/api/plugins/fetch/:author/:plugin` - POSTs to this endpoint let you fetch
  the plugin named `:plugin` by `:author` from the library if you have the
  correct permissions or it is a public library.
- POST `/api/plugins/upload` - POSTs to this endpoint let you upload a plugin to the
  server to add it to the library or update an existing plugin.
  - Body Contents:
    - `plugin` - The plugin in JSON format as returned by
      $tw.wiki.getTiddler(pluginName)

## Federation

- `/api/federation/socket` - This is the endpoint used by remote servers to
  establish a websocket connection to this server for other federation things.
  Like syncing wikis and opening chat portals.
  - This doesn't use GET or POST, instead this route is used for incoming
    websocket connections from remote servers.
