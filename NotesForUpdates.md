Stuff that we need!!

[ ] find name function
[ ] test route '/'
[ ] test route '/facicon.ico'
[ ] test routes for other wikis
[ ] test plugin library routes
  [ ] test get plugin
  [ ] test list plugins
  [ ] test push plugin
[ ] test push route
[ ] test fetch route
  [ ] test fetch list (returns list of titles)
  [ ] test fetch tiddlers (returns full tiddlers)

We don't embed media/etc in tiddler itself - file blobs with urls
Need to be user friendly names
[ ] file server routes (/files/)
  * This should probably be designed to be drop-in compatible with DBs.
  [ ] globally available files - stuff we put
    - /files/blah.jpg
  [ ] files only available in a wiki
    - /person/wiki/files/blah
  [ ] (maybe) files only available to each person
    - /person/files/blah

- Add the `setWikiPermissions` message
  - As a sub part you need to be able to view the current permissions settings
    for the wiki.
  - Figure out how to allow transfer ownership to someone else without inviting
    badness.
  - TEST IT
- if the index wiki is served on / and public isn't set to 'yes' than it gets
  into an infinite forwarding loop.
- We are missing an endpoint to remove users. I don't know if it is required.
  Letting people remove their account is probably a good idea.
- figure out how we should rate limit requests and messages.
- Somehow the browser should detect if you aren't logged in and not spam the
  server with retries because the server doesn't acknowledge any messages from
  connections that aren't logged in.
- implement the security and access controls for namespaced wikis. At the
  moment it is just a path to a wiki and the permissions are all done on a
  per-wiki basis. There are also no controls for who can and can't create wikis
  in different namespaces. So anyone who can create wikis could create a wiki
  in `/inmysocks/foo`.
- The federation websocket server and the handlers for it need to be added to
  `js/websocketserver.js`
- Consider finding a way to set up the websocket server part of
  `js/websocketserver.js` so that you can have a list of sever descriptions
  that you pass it and it adds each one in the same way that adding routes
  works for the http part.
- Make everything handle not having a settings.json file in the indexwiki in
  the loadWiki function in ServerSide.js. Something about the ws-server section
  of the settings not existing at that point if there is no settings.json file
- The `/*/favicon.ico` route may be cutting off the last bit of routes.
  `/bob/joe/eddie` tried to get `/bob/joe/favicon.ico`
- The server alerts shown in the browser shouldn't be saved to the file system
- It isn't showing only the wikis you are allowed to see after you log out.
  This may not be a problem.
  - The `type` parameter in the API calls is unnecessary because the type is
    determined by the URL, so remove them. This is low priority.
