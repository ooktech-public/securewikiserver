const checkPermission = require('../js/checkPermissions.js')
const wikiPermissions = require('./js/checkPermissions.js').wikiPermissions
/*
  This is for uploading media files
*/
module.exports = [
  {
    verb: 'post',
    route: '/api/permissions',
    handler: function (request, response) {
      // You can't use this to change ownership yet.
      const authorised = checkPermission(request.get('x-wiki-name'), response, 'setWikiPermissions')
      if (authorised) {
        const fullName = request.get('x-wiki-name')
        const nameParts = fullName.split('/')
        let wikiPermissionsObject = wikiPermissions.wikis || {}
        nameParts.forEach(function(part) {
          wikiPermissionsObject = wikiPermissionsObject[part] || {}
        })
        wikiPermissionsObject = wikiPermissionsObject.__permissions || {}
        // Setting owners comes later
        // Setting permissions by login levels comes later
        let update = false
        // public
        if (typeof request.body.public !== 'undefined') {
          if ((request.body.public === 'yes' || request.body.public === 'no') && wikiPermissionsObject.public !== request.body.pubilc) {
            update = true
            wikiPermissionsObject.public = request.body.public
          }
        }
        // editors
        if (typeof request.body.editors !== 'undefined') {
          if (Array.isArray(request.body.editors)) {
            update = true
            wikiPermissionsObject.editors = request.body.editors
          }
        }
        // viewers
        if (typeof request.body.viewers !== 'undefined') {
          if (Array.isArray(request.body.viewers)) {
            update = true
            wikiPermissionsObject.viewers = request.body.viewers
          }
        }
        // fetchers
        if (typeof request.body.fetchers !== 'undefined') {
          if (Array.isArray(request.body.fetchers)) {
            update = true
            wikiPermissionsObject.fetchers = request.body.fetchers
          }
        }
        // pullers
        if (typeof request.body.pullers !== 'undefined') {
          if (Array.isArray(request.body.pullers)) {
            update = true
            wikiPermissionsObject.pullers = request.body.pullers
          }
        }
        if (update) {
          let baseDir = process.pkg?path.dirname(process.argv[0]):process.cwd()
          if (settings.wikiPathBase === 'homedir') {
            baseDir = os.homedir();
          } else if (settings.wikiPathBase === 'cwd' || !settings.wikiPathBase) {
            baseDir = process.pkg?path.dirname(process.argv[0]):process.cwd()
          } else {
            baseDir = path.resolve(settings.wikiPathBase)
          }
          const localPath = path.resolve(baseDir,settings.localWikiPermissionsPath)
          settings.saveSetting(wikiPermissions, localPath, false)
        }
        response.writeHead(200)
        response.end()
      } else {
        response.writeHead(403)
        response.end()
      }
    }
  }
]
