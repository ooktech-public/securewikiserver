const settings = require('../LoadConfig.js').settings
const wiki = require('../wiki.js')
const checkPermission = require('../js/checkPermissions.js')
let routes1 = []
let routes2 = []
if (settings.API.enableFetch === 'yes') {
  routes1 = [
    {
      verb: 'post',
      route: '/api/fetch/list',
      handler: function(request, response) {
        let list
        let data = {}
        if (settings.reverseProxy !== 'yes') {
          response.setHeader('Access-Control-Allow-Origin', '*')
        }
        response.writeHead(200, {"Content-Type": "application/json"});
        try {
          const bodyData = JSON.parse(request.body.message)
          const hasAccess = checkPermission(bodyData.fromWiki, response, 'fetch')
          if (hasAccess) {
            if (bodyData.filter && bodyData.fromWiki) {
              // Make sure that the wiki is listed
              if (request.wiki.tw.settings.wikis[bodyData.fromWiki] || bodyData.fromWiki === 'RootWiki') {
                if (!request.wiki.tw.Bob.Wikis) {
                  request.wiki.tw.ServerSide.loadWiki(bodyData.fromWiki, request.wiki.tw.boot.wikiPath);
                }
                // If the wiki isn't loaded than load it
                if (!request.wiki.tw.Bob.Wikis[bodyData.fromWiki]) {
                  request.wiki.tw.ServerSide.loadWiki(bodyData.fromWiki, request.wiki.tw.settings.wikis[bodyData.fromWiki]);
                } else if (wiki.tw.Bob.Wikis[bodyData.fromWiki].State !== 'loaded') {
                  request.wiki.tw.ServerSide.loadWiki(bodyData.fromWiki, request.wiki.tw.settings.wikis[bodyData.fromWiki]);
                }
                // Make sure that the wiki exists and is loaded
                if (request.wiki.tw.Bob.Wikis[bodyData.fromWiki]) {
                  if (request.wiki.tw.Bob.Wikis[bodyData.fromWiki].State === 'loaded') {
                    // Make a temp wiki to run the filter on
                    const tempWiki = new wiki.tw.Wiki();
                    request.wiki.tw.Bob.Wikis[bodyData.fromWiki].tiddlers.forEach(function(internalTitle) {
                      const tiddler = request.wiki.tw.wiki.getTiddler(internalTitle);
                      if (tiddler) {
                        const newTiddler = JSON.parse(JSON.stringify(tiddler));
                        newTiddler.fields.modified = request.wiki.tw.utils.stringifyDate(new Date(newTiddler.fields.modified));
                        newTiddler.fields.created = request.wiki.tw.utils.stringifyDate(new Date(newTiddler.fields.created));
                        newTiddler.fields.title = newTiddler.fields.title.replace('{' + bodyData.fromWiki + '}', '');
                        // Add all the tiddlers that belong in wiki
                        tempWiki.addTiddler(new request.wiki.tw.Tiddler(newTiddler.fields));
                      }
                    })
                    // Use the filter
                    list = tempWiki.filterTiddlers(bodyData.filter);
                  }
                }
              }
              const info = {}
              list.forEach(function(title) {
                const tempTid = tempWiki.getTiddler(title)
                info[title] = {}
                if (bodyData.fieldList) {
                  bodyData.fieldList.split(' ').forEach(function(field) {
                    info[title][field] = tempTid.fields[field]
                  })
                } else {
                  info[title]['modified'] = tempTid.fields.modified
                }
              })
              // Send the tiddlers
              data = {list: list, info: info}
              data = JSON.stringify(data) || "{}";
              response.end(data);
            }
          } else {
            // Don't have access
            response.status(403);
            response.end(false);
          }
        } catch (e) {
          console.log(e)
          //data = JSON.stringify(data) || "";
          response.status(403);
          response.end();
        }
        response.status(403);
        response.end()
      }
    },
    {
      verb: 'post',
      route: '/api/fetch',
      handler: function(request, response) {
        let list
        let data = {}
        if (settings.reverseProxy !== 'yes') {
          response.setHeader('Access-Control-Allow-Origin', '*')
        }
        response.writeHead(200, {"Content-Type": "application/json"});
        try {
          const bodyData = JSON.parse(request.body.message)
          const hasAccess = checkPermission(bodyData.fromWiki, response, 'fetch')
          if (hasAccess) {
            if (bodyData.filter && bodyData.fromWiki) {
              // Make sure that the wiki is listed
              if (request.wiki.tw.settings.wikis[bodyData.fromWiki] || bodyData.fromWiki === 'RootWiki') {
                if (!request.wiki.tw.Bob.Wikis) {
                  request.wiki.tw.ServerSide.loadWiki(bodyData.fromWiki, request.wiki.tw.boot.wikiPath);
                }
                // If the wiki isn't loaded than load it
                if (!request.wiki.tw.Bob.Wikis[bodyData.fromWiki]) {
                  request.wiki.tw.ServerSide.loadWiki(bodyData.fromWiki, request.wiki.tw.settings.wikis[bodyData.fromWiki]);
                } else if (request.wiki.tw.Bob.Wikis[bodyData.fromWiki].State !== 'loaded') {
                  request.wiki.tw.ServerSide.loadWiki(bodyData.fromWiki, request.wiki.tw.settings.wikis[bodyData.fromWiki]);
                }
                // Make sure that the wiki exists and is loaded
                if (request.wiki.tw.Bob.Wikis[bodyData.fromWiki]) {
                  if (request.wiki.tw.Bob.Wikis[bodyData.fromWiki].State === 'loaded') {
                    // Make a temp wiki to run the filter on
                    const tempWiki = new request.wiki.tw.Wiki();
                    request.wiki.tw.Bob.Wikis[bodyData.fromWiki].tiddlers.forEach(function(internalTitle) {
                      const tiddler = request.wiki.tw.wiki.getTiddler(internalTitle);
                      if (tiddler) {
                        const newTiddler = JSON.parse(JSON.stringify(tiddler));
                        newTiddler.fields.modified = request.wiki.tw.utils.stringifyDate(new Date(newTiddler.fields.modified));
                        newTiddler.fields.created = request.wiki.tw.utils.stringifyDate(new Date(newTiddler.fields.created));
                        newTiddler.fields.title = newTiddler.fields.title.replace('{' + bodyData.fromWiki + '}', '');
                        // Add all the tiddlers that belong in wiki
                        tempWiki.addTiddler(new request.wiki.tw.Tiddler(newTiddler.fields));
                      }
                    })
                    // Use the filter
                    list = tempWiki.filterTiddlers(bodyData.filter);
                  }
                }
              }
              const tiddlers = {}
              const info = {}
              list.forEach(function(title) {
                const tempTid = tempWiki.getTiddler(title)
                tiddlers[title] = tempTid
                info[title] = {}
                if (bodyData.fieldList) {
                  bodyData.fieldList.split(' ').forEach(function(field) {
                    info[title][field] = tempTid.fields[field];
                  })
                } else {
                  info[title]['modified'] = tempTid.fields.modified;
                }
              })
              // Send the tiddlers
              data = {list: list, tiddlers: tiddlers, info: info}
              data = JSON.stringify(data) || "{}";
              response.end(data);
            }
          } else {
            // Don't have access
            data = "{}";
            response.status(403);
            response.end(false);
          }
        } catch (e) {
          data = JSON.stringify(data) || "{}";
          response.end(data);
        }
        response.status(403);
        response.end("{}")
      }
    }
  ]
}

if (settings.API.enablePush === 'yes') {
  let routes2 = [
    {
      verb: 'post',
      route: '/api/push',
      handler: function(request, response) {
        if (settings.reverseProxy !== 'yes') {
          response.setHeader('Access-Control-Allow-Origin', '*')
        }
        try {
          const bodyData = JSON.parse(request.body.message)
          // Make sure that the token sent here matches the https header
          // and that the token has push access to the toWiki
          const allowed = checkPermission(bodyData.toWiki, response, 'push')
          if (allowed) {
            if (request.wiki.tw.settings.wikis[bodyData.toWiki] || bodyData.toWiki === 'RootWiki') {
              request.wiki.tw.ServerSide.loadWiki(bodyData.toWiki, request.wiki.tw.settings.wikis[bodyData.toWiki]);
              // Make sure that the wiki exists and is loaded
              if (request.wiki.tw.Bob.Wikis[bodyData.toWiki]) {
                if (request.wiki.tw.Bob.Wikis[bodyData.toWiki].State === 'loaded') {
                  if (bodyData.tiddlers && bodyData.toWiki) {
                    Object.keys(bodyData.tiddlers).forEach(function(title) {
                      bodyData.tiddlers[title].fields.modified = request.wiki.tw.utils.stringifyDate(new Date(bodyData.tiddlers[title].fields.modified));
                      bodyData.tiddlers[title].fields.created = request.wiki.tw.utils.stringifyDate(new Date(bodyData.tiddlers[title].fields.created));
                      request.wiki.tw.syncadaptor.saveTiddler(bodyData.tiddlers[title], bodyData.toWiki);
                    });
                    response.writeHead(200)
                    response.end()
                  }
                }
              }
            }
          } else {
            response.writeHead(400)
            response.end()
          }
        } catch (e) {
          response.writeHead(403)
          response.end()
        }
      }
    }
  ]
}

module.exports = routes1.concat(routes2)
