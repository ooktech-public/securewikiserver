const checkPermission = require('../js/checkPermissions.js')
const wiki = require('../wiki.js')
/*
  This is for uploading media files
*/
module.exports = [
  {
  verb: 'post',
  route: '/upload',
  handler: function (request, response) {
    const authorised = checkPermission(request.get('x-wiki-name'), response, 'upload')
    if (authorised) {
      let body = ''
      request.on('data', function (data) {
        body += data
        if (body.length > 10e6) {
          request.connection.destroy()
        }
      });
      // Set a timeout to close the request in case of inactivity
      request.setTimeout(5000, function() {
        request.writeHead(400);
        request.end();
      });
      request.on('end', function () {
        const parsedBody = JSON.parse(body)
        const filesPath = path.join(request.wiki.tw.Bob.Wikis[parsedBody.wiki].wikiPath, 'files')
        console.log('Uploaded ',filesPath,'/',parsedBody.tiddler.fields.title,' for ',parsedBody.wiki)
        const buf = Buffer.from(parsedBody.tiddler.fields.text,'base64')
        //Make sure that the folder exists
        try {
          fs.mkdirSync(filesPath)
        } catch (e) {
          console.log(e)
        }
        fs.writeFile(path.join(filesPath, parsedBody.tiddler.fields.title), buf, function(error) {
          if (error) {
            console.log(error)
          } else {
            console.log("C'est fini!")
            return true
          }
        })
      })
      // TODO return some sort of response!
      response.writeHead(200)
      response.end()
    } else {
      response.writeHead(403)
      response.end()
    }
  }
}
]
