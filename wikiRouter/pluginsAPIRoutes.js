const settings = require('../LoadConfig.js').settings
const wiki = require('../wiki.js')
if (settings.API.pluginLibrary === 'yes') {
  // Check if the access token gives the privlidegs needed to push plugins to
  // the library
  function canPushPlugin (bodyData, response) {
    if (settings.admin) {
      if (settings.admin.pushPlugins) {
        try {
          const key = require('../js/loadSecrets.js')
          const decoded = jwt.verify(bodyData.token, key)
          if (decoded) {
            if (settings.admin.pushPlugins.indexOf(decoded.level) !== -1) {
              return true
            }
          }
        } catch (e) {
          return false
        }
      }
    }
    return false
  }
  // List the available plugins
  /*
    The response is a pluginInfoList where:
    pluginInfoList = [pluginInfo]
    pluginInfo = {name, tiddlerName, version, readme}
  */
  const getPluginList = function () {
    const pluginList = []
    if (typeof settings.pluginsPath === 'string') {
      const pluginsPath = path.resolve(baseDir,settings.pluginsPath)
      try {
        if(fs.existsSync(pluginsPath)) {
          const pluginAuthors = fs.readdirSync(pluginsPath)
          pluginAuthors.forEach(function (author) {
            const pluginAuthorPath = path.join(pluginsPath, './', author)
            if (fs.statSync(pluginAuthorPath).isDirectory()) {
              const pluginAuthorFolders = fs.readdirSync(pluginAuthorPath)
              for(let t=0; t<pluginAuthorFolders.length; t++) {
                const fullPluginFolder = path.join(pluginAuthorPath,pluginAuthorFolders[t])
                const pluginFields = wiki.tw.loadPluginFolder(fullPluginFolder)
                if(pluginFields) {
                  let readme = ""
                  let readmeText = ''
                  try {
                    // Try pulling out the plugin readme
                    const pluginJSON = JSON.parse(pluginFields.text).tiddlers
                    readme = pluginJSON[Object.keys(pluginJSON).filter(function(title) {
                      return title.toLowerCase().endsWith('/readme')
                    })[0]]
                  } catch (e) {
                    console.log('Error parsing plugin', e)
                  }
                  if (readme) {
                    readmeText = readme.text
                  }
                  const nameParts = pluginFields.title.split('/')
                  const name = nameParts[nameParts.length-2] + '/' + nameParts[nameParts.length-1]
                  const listInfo = {
                    name: name,
                    description: pluginFields.description,
                    tiddlerName: pluginFields.title,
                    version: pluginFields.version,
                    author: pluginFields.author,
                    readme: readmeText
                  }
                  pluginList.push(listInfo)
                }
              }
            }
          })
        }
      } catch (e) {
        console.log('Failed to load plugin list', e);
      }
    }
    return pluginList
  }

  modules.exports = [
    {
      verb: 'post',
      route: '/api/plugins/list',
      handler: function(request, response) {
        // Get a list of available plugins
        const pluginList = getPluginList()
        if (settings.reverseProxy !== 'yes') {
          response.setHeader('Access-Control-Allow-Origin', '*')
        }
        response.status(200)
        response.end(JSON.stringify(pluginList))
      }
    },
    {
      verb: 'post',
      route: '/api/plugins/fetch/:author/:pluginName',
      handler: function(request, response) {
        // Fetch a specific plugin
        const wikiPluginsPath = path.resolve(baseDir, settings.pluginsPath, request.params.author, request.params.pluginName)
        // Make sure that we don't allow url tricks to access things people
        // aren't supposed to
        if (wikiPluginsPath.startsWith(path.resolve(baseDir, settings.pluginsPath))) {
          const pluginFields = wiki.tw.loadPluginFolder(wikiPluginsPath)
          if (pluginFields) {
            if (settings.reverseProxy !== 'yes') {
              response.setHeader('Access-Control-Allow-Origin', '*')
            }
            response.status(200)
            response.end(JSON.stringify(pluginFields))
          } else {
            response.status(403)
            response.end()
          }
        } else {
          response.status(403)
          response.end()
        }
      }
    },
    {
      verb: 'post',
      route: '/api/plugins/upload',
      handler: function(request, response) {
        // Add a plugin to the library
        try {
          const bodyData = JSON.parse(request.body.message)
          const canPush = canPushPlugin(bodyData, response)
          if (canPush) {
            // Save plugin
            const fs = require('fs')
            const path = require('path')
            const tempWiki = new wiki.tw.Wiki()
            const pluginName = bodyData.plugin.title.replace('^$:/plugins')
            // Make sure plugin folder exists
            const pluginFolderPath = path.resolve(baseDir, request.wiki.tw.settings.pluginsPath, pluginName)
            const error = request.wiki.tw.utils.createDirectory(pluginFolderPath)

            Object.keys(bodyData.plugin.text).forEach(function(tidTitle) {
              const tiddler = new wiki.tw.Tiddler(bodyData.plugin.text[tidTitle], {title: tidTitle})
              if (tiddler) {
                tempWiki.addTiddler(tiddler)
                const title = tiddler.fields.title;
                const filepath = pluginFolderPath + path.sep + pluginName
                // Save the tiddler as a self contained templated file
                const content = tempWiki.renderTiddler("text/plain", "$:/core/templates/tid-tiddler", {variables: {currentTiddler: title}});
                // If we aren't passed a path
                fs.writeFile(filepath,content,{encoding: "utf8"},function (err) {
                  if(err) {
                    console.log(err);
                  } else {
                    console.log('saved file', filepath)
                  }
                });
              }
            })
            // Build the plugin.info file
            const pluginInfo = {}
            Object.keys(bodyData.plugin).forEach(function(field) {
              if (field !== 'text') {
                pluginInfo[field] = bodyData.plugin[field]
              }
            })
            filepath = pluginFolderPath + path.sep + 'plugin.info'
            fs.writeFile(filepath, JSON.stringify(pluginInfo, null, 2), function (err) {
              if (err) {
                console.log(err)
              } else {
                console.log('save plugin.info')
              }
            })
          }
        } catch (e) {

        }
      }
    }
  ]
} else {
  module.exports = []
}
