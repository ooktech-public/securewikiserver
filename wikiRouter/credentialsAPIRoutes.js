const settings = require('../LoadConfig.js').settings
settings.API = settings.API || {}
const adminHandlers = require('../js/websocketadminhandlers.js')
if (settings.API.updatePasswords === 'yes' || true) {
  const checkPermission = require('../js/checkPermissions.js')
  const updateCredentials = require('../addperson.js')
  const bcrypt = require('bcrypt')

  module.exports = [
    {
      verb: 'post',
      route: '/api/credentials/add/:userName',
      handler: function (request, response) {
        /*
          This is to add new people and set their passwords
        */
        // Check to make sure that the token in the headers is correct
        // Here the wikiname isn't important and is ignored
        const authorised = checkPermission(request.get('x-wiki-name'), response, 'addPerson')
        if (authorised) {
          const bodyData = request.body
          // Make sure that the post request contains the new password
          const name = request.params.userName || false
          const password = bodyData.pwd || false
          const level = bodyData.lvl || 'Guest'
          // This is a special case that handles the administartor account
          // creation when the server is beting set up.
          if (!response.hasAdmin) {
            // Special stuff!
            // Make the admin owner of the admin wiki
            adminHandlers.setWikiOwner({newOwner: name, wikiName: 'Admin'}).then(function() {
              return adminHandlers.setPublic({public: false, wikiName: 'Admin'})
            }).then(function() {
              return adminHandlers.setWikiOwner({newOwner: name, wikiName: 'RootWiki'})
            }).then(function() {
              return updateCredentials.saveCredentials(name, password, level)
            }).then(function() {
              return require('../LoadConfig.js').saveSetting({hasAdmin: true})
            }).then(function() {
              response.end('Success')
            }).catch(function(err) {
              console.log(err)
            })
          } else {
            // Add the person with the password as long as the above parts are ok
            updateCredentials.saveCredentials(name, password, level)
            response.end('Success')
          }
        } else {
          response.end('')
        }
      }
    },
    {
      verb: 'post',
      route: '/api/credentials/update/:userName',
      handler: function (request, response) {
        /*
          This is for people to change their passwords
        */
        // Check to make sure that the token in the headers is correct
        // Here the wikiname isn't important and is ignored
        const authorised = checkPermission(request.get('x-wiki-name'), response, 'changePassword')
        if (authorised) {
          // Make sure that the post request contains the new password
          const name = request.params.userName || false
          const oldPassword = request.body.pwd || false
          const password = request.body.new || false
          const level = request.body.level || 'Guest'
          if (name && password) {
            // Update the password as long as the above parts are ok
            updateCredentials.saveCredentials(name, password, level, true, oldPassword)
            response.writeHead(200)
            response.end()
          } else {
            response.writeHead(403).end()
          }
        } else {
          response.writeHead(403).end()
        }
      }
    },
    {
      verb: 'post',
      route: '/api/credentials/delete/:userName',
      handler: function (request, response) {
        // Check to make sure that the token and headers are correct
        const authorised = checkPermission(request.params.userName, response, 'deleteUser')
        if (authorised) {
          const name = request.params.userName || false
          const password = request.body.pwd || false
          // Something here
          response.writeHead(200).end("Delete user unimplemented")
        }
      }
    }
  ]
} else {
  module.exports = []
}
