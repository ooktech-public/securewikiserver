const checkPermission = require('../js/checkPermissions.js')
const unauthorised = "<html><p>You don't have the authorisation to view this wiki.</p> <p><a href='/'>Return to login</a></p></html>"

module.exports = [
  {
    verb: 'get',
    route: '/api/status',
    handler: function(request, response) {
      const jwt = require('jsonwebtoken')
      const token = request.wiki.tw.Bob.getCookie(request.headers.cookie, 'token');
      let decoded = false
      if (token) {
        try {
          const key = require('../js/loadSecrets.js')
          decoded = jwt.verify(token, key)
        } catch (e) {
          decoded = false
        }
      }

      const authorised = true;

      // build the status object
      // TODO make the available wikis list show owned, editable, viewable etc status of each wiki
      const status = {
        logged_in: (!!decoded) ? 'yes' : 'no',
        username: decoded.name || undefined,
        authentication_level: decoded.level || "Guest",
        tiddlywiki_version: request.wiki.tw.version,
        bob_version: request.wiki.tw.Bob.version,
        read_only: checkPermission(request.headers['x-wiki-name'], response, 'edit') ? 'no' : 'yes',
        available_wikis: request.wiki.tw.ServerSide.getViewableWikiList({decoded: decoded}),
        available_themes: request.wiki.tw.ServerSide.getViewableThemesList({decoded: decoded}),
        available_plugins: request.wiki.tw.ServerSide.getViewablePluginsList({decoded: decoded}),
        available_languages: request.wiki.tw.ServerSide.getViewableLanguagesList({decoded: decoded}),
        available_editions: request.wiki.tw.ServerSide.getViewableEditionsList({decoded: decoded}),
        settings: request.wiki.tw.ServerSide.getViewableSettings({decoded: decoded})
      }
      response.writeHead(200, {"Content-Type": "application/json", "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": "true", "Access-Control-Allow-Headers": "*"});
      response.end(JSON.stringify(status));
    }
  },
  {
    verb: 'get',
    route: '/',
    handler: function(request,response) {
      // Get the root wiki
      // Add a check to make sure that the person logged in is authorised
      // to open the wiki.
      const authorised = checkPermission('RootWiki', response, 'view')
      if (authorised) {
        // Load the wiki
        request.wiki.tw.ServerSide.loadWiki('RootWiki', request.wiki.tw.boot.wikiPath)
        // Get the raw html to send
        const text = request.wiki.tw.ServerSide.prepareWiki('RootWiki', true)
        // Send the html to the server
        response.writeHead(200).end(text,"utf8")
      } else {
        response.writeHead(200).end(unauthorised, "utf8")
      }
    }
  },
  {
    verb: 'get',
    route: '/favicon.ico',
    handler: function(request,response) {
      // Add a check to make sure that the person logged in is authorised
      // to open the wiki.
      const authorised = checkPermission('RootWiki', response, 'view')
      if (authorised) {
        response.writeHead(200, {"Content-Type": "image/x-icon"})
        const buffer = request.wiki.tw.wiki.getTiddlerText("$:/favicon.ico","")
        response.end(buffer,"base64")
      }
    }
  },
  {
    verb: 'get',
    route: '/:wikiName',
    handler: function(request, response) {
      // Make sure that the logged in person is authorised to access the wiki
      const authorised = checkPermission(request.params.wikiName, response, 'view')
      let text = "<html><p>No wiki found! Either there is no usable tiddlywiki.info file in the listed location or it isn't listed.</p></html>"
      if (authorised) {
        // Make sure we have loaded the wiki tiddlers.
        // This does nothing if the wiki is already loaded.
        const exists = request.wiki.tw.ServerSide.loadWiki(request.params.wikiName, request.wiki.tw.settings.wikis[request.params.wikiName]);
        if (exists) {
          // If servePlugin is not false than we strip out the filesystem
          // and tiddlyweb plugins if they are there and add in the
          // multiuser plugin.
          request.wiki.tw.settings['ws-server'] = request.wiki.tw.settings['ws-server'] || {}
          const servePlugin = !request.wiki.tw.settings['ws-server'].servePlugin || request.wiki.tw.settings['ws-server'].servePlugin !== false
          // Get the full text of the html wiki to send as the response.
          text = request.wiki.tw.ServerSide.prepareWiki(request.params.wikiName, servePlugin);
        }
        //response.writeHead(200, {"Content-Type": "text/html"});
        response.end(text,"utf8");
      } else {
        response.end(unauthorised, "utf8")
      }
    }
  },
  {
    verb: 'get',
    route: '/:wikiName/favicon.ico',
    handler: function (request, response) {
      // Add a check to make sure that the person logged in is authorised
      // to open the wiki.
      const authorised = checkPermission(request.params.wikiName, response, 'view')
      if (authorised) {
        const buffer = request.wiki.tw.Bob.Wikis[request.params.wikiName].wiki.getTiddlerText("$:/favicon.ico","")
        response.end(buffer,"base64")
      } else {
        response.writeHead(403)
        response.end()
      }
    }
  },
  {
    verb: 'get',
    route: '/*/favicon.ico',
    handler: function(request, response) {
      const wikiName = request.url.slice(1, -12)
      const authorised = checkPermission(wikiName, response, 'view')
      if (authorised) {
        response.writeHead(200, {"Content-Type": "image/x-icon"})
        const buffer = request.wiki.tw.Bob.Wikis[request.params.wikiName].wiki.getTiddlerText("$:/favicon.ico","")
        response.end(buffer,"base64")
      } else {
        response.writeHead(403)
        response.end()
      }
    }
  },
  {
    verb: 'get',
    route: '/*',
    handler: function(request, response) {
      const wikiName = request.url.slice(1)
      const authorised = checkPermission(wikiName, response, 'view')
      if (authorised) {
        let text = "<html><p>No wiki found! Either there is no usable tiddlywiki.info file in the listed location or it isn't listed.</p></html>"
        // Get wiki path
        const nameParts = wikiName.split('/')
        let pathsObject = request.wiki.tw.settings.wikis || {}
        nameParts.forEach(function(part) {
          pathsObject = pathsObject[part] || "";
        })
        // Make sure we have loaded the wiki tiddlers.
        // This does nothing if the wiki is already loaded.
        const exists = request.wiki.tw.ServerSide.loadWiki(wikiName, pathsObject);
        if (exists) {
          // If servePlugin is not false than we strip out the filesystem
          // and tiddlyweb plugins if they are there and add in the
          // multiuser plugin.
          const servePlugin = !request.wiki.tw.settings['ws-server'].servePlugin || request.wiki.tw.settings['ws-server'].servePlugin !== false;
          // Get the full text of the html wiki to send as the response.
          text = request.wiki.tw.ServerSide.prepareWiki(wikiName, servePlugin);
        }
        response.writeHead(200, {"Content-Type": "text/html"});
        response.end(text,"utf8");
      } else {
        response.writeHead(403, {"Content-Type": "text/html"}).end(unauthorised, "utf8")
      }
    }
  }
]
