const checkPermission = require('../js/checkPermissions.js')
/*
  This is used to load a media file from the server
  It needs to be updated to support files for individual wikis
*/
function loadMediaFile(request, response, wikiName) {
  request.wiki.tw.settings.mimeMap = request.wiki.tw.settings.mimeMap || {
    '.aac': 'audio/aac',
    '.avi': 'video/x-msvideo',
    '.csv': 'text/csv',
    '.doc': 'application/msword',
    '.epub': 'application/epub+zip',
    '.gif': 'image/gif',
    '.html': 'text/html',
    '.htm': 'text/html',
    '.ico': 'image/x-icon',
    '.jpg': 'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.mp3': 'audio/mpeg',
    '.mpeg': 'video/mpeg',
    '.oga': 'audio/ogg',
    '.ogv': 'video/ogg',
    '.ogx': 'application/ogg',
    '.png': 'image/png',
    '.svg': 'image/svg+xml',
    '.weba': 'audio/weba',
    '.webm': 'video/webm',
    '.wav': 'audio/wav'
  }
  const authorised = checkPermission(wikiName, response, 'view')
  if (authorised) {
    //Make sure that the file type is listed in the mimeMap
    if (request.wiki.tw.settings.mimeMap[path.extname(request.params.filePath).toLowerCase()]) {
      const fileFolderPath = path.resolve(request.wiki.tw.Bob.Wikis[wikiName].wikiPath, 'files')
      const file = path.join(fileFolderPath, decodeURIComponent(request.params.filePath))
      // Make sure that there aren't any sneaky things like '../../../.ssh' in
      // the resolved file path.
      if (file.startsWith(fileFolderPath)) {
        fs.access(file, fs.constants.F_OK, function (error) {
          if (error) {
            console.log(error)
            // File doesn't exist, reply with 404 or something like that
          } else {
            // File exists! Reply with the file.
            fs.readFile(file, function (err, data) {
              if (err) {
                // Problem, return 404
                response.writeHead(404)
                response.end()
              } else {
                // return file with mimetype
                response.writeHead(200, {"Content-Type": request.wiki.tw.settings.mimeMap[path.extname(request.params.filePath).toLowerCase()]})
                response.end(data)
              }
            })
          }
        })
      }
    } else {
      response.writeHead(404)
    }
  } else {
    response.writeHead(403)
  }
}

module.exports = [{
  verb: 'get',
  route: '/files/:filepath',
  handler: function (request, response) {
    loadMediaFile(request, response, "RootWiki")
  }
},
{
  verb: 'get',
  route: '/:wikiName/files/:filePath',
  handler: function (request, response) {
    loadMediaFile(request, response, request.params.wikiName)
  }
},
{
  verb: 'get',
  route: '/*/files/*',
  handler: function (request, response) {
    function findName(url) {
      const pieces = url.split('/')
      let name = pieces[0]
      let settingsObj = request.wiki.tw.settings.wikis[name]
      for (let i = 1; i < pieces.length; i++) {
        if (typeof settingsObj[pieces[i]] === 'object') {
          name = name + '/' + pieces[i]
          settingsObj = settingsObj[pieces[i]]
        } else if (typeof settingsObj[pieces[i]] === 'string') {
          name = name + '/' + pieces[i]
          break
        } else {
          break
        }
      }
      return name
    }
    const wikiName = findName(request.url.replace(/^\//, ''))
    const urlPieces = request.url.replace(/^\//, '').split('/')
    const filePath = urlPieces.slice(urlPieces.indexOf('files')+1).join('/')
    request.params.wikiName = wikiName
    request.params.filePath = filePath
    loadMediaFile(request, response, wikiName)
  }
}
]
