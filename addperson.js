/*
  This is what you use to add a new person to the server. It adds them with a
  name and password.

  So we should make a way for people to change their passwords after they
  authenticate.

  Usage:

  node addperson.js name password level

  name and password are required, level is optional and defaults to guest

  Stuff this shows:
  - Getting and using commandline arguments
  - Hash passwords using bcrypt
  - Create folders if they don't already exist
  - Save information to a file and set appropriate file permissions
*/

const fs = require('fs')
const path = require('path')
const os = require('os')
const bcrypt = require('bcrypt')
const PouchDB = require('pouchdb')
const peopleDB = new PouchDB('db/people')
const settings = require('./LoadConfig.js').settings

if (require) {
  if (require.main === module) {
    if (process.argv.length < 3) {
      console.log('Usage: node ./addperson.js name password level')
    } else {
      const name = process.argv[2]
      const password = process.argv[3]
      let level = 'guest'
      if (process.argv.length >= 4) {
        level = process.argv[4]
      }
      let update = false
      if (process.argv.length >= 5) {
        if (process.argv[5]) {
          update = true
        }
      }
      saveCredentials(name, password, level, update)
    }
  }
}

function changePassword (name, password, oldPasswod) {
  // Check to make sure that the persons info alreadys exist on the server
  peopleDB.get(name).catch(function(err) {
    throw err
  }).then(function(personData) {
    const match = bcrypt.compareSync(oldPassword, personData.hash)
    if (match) {
      const saltRounds = settings.saltRounds || 10
      // Update the existing person
      personData.hash = bcrypt.hashSync(password, Number(saltRounds))
      return peopleDB.put(personData)
    } else {
      throw 'Passwords do not match'
    }
  }).catch(function(err) {
    if (err === 'Passwords do not match') {
      console.log('Old password did not match')
    } else {
      console.log(err)
    }
  })
}

/*
  name - the name used to log in
  password - the desired password
  level - the server access level for the login
  update - if this is true than it will overwite any existing info for the
    login for the name given, otherwise you will get an error if you try to
    make login details using a name that already exists.
*/
function saveCredentials (name, password, level, update) {
  // Check to make sure that the persons info doesn't already exist on the
  // server
  peopleDB.get(name).catch(function(err) {
    if (err.name == 'not_found') {
      return {
        '_id': name,
        'level': level
      }
    } else {
      throw err
    }
  }).then(function(personData) {
    const saltRounds = settings.saltRounds || 10
    if (typeof personData._rev === 'undefined' && !update) {
      // A new person to add
      personData.hash = bcrypt.hashSync(password, Number(saltRounds))
      personData.level = level
      return peopleDB.put(personData)
    } else if (typeof personData._rev !== 'undefined' && update) {
      // Update the existing person
      personData.hash = bcrypt.hashSync(password, Number(saltRounds))
      personData.level = level
      return peopleDB.put(personData)
    } else if (!update && personData._rev !== 'undefined') {
      throw 'Person Exists'
    } else {
      throw 'No Person To Update'
    }
  }).catch(function(err) {
    if (err === 'Person Exists') {
      console.log('Person exists, can\'t make two people with same name')
    } else if (err === 'No Person To Update') {
      console.log('Can\'t update a person who doesn\'t have an entry')
    } else if (err === 'Passwords do not match') {
      console.log('Old password did not match')
    } else {
      console.log(err)
    }
  })
}

module.exports = {}
module.exports.saveCredentials = saveCredentials
module.exports.changePassword = changePassword
