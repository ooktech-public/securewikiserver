- `certPath` - the path to the https certificate for the server
- `disableBrowserAlerts` - if this is set to `yes` no alerts will be set to
  connected browsers.
- `disableFileWatchers` - If this is set to `yes` the file system monitor is
  disabled and changes on the file system are not propagated to the browser.
- `editionsPath` - The path where editions are stored, it can be absolute or
  relative to the `wikisPathBase`
- `enableFederation` - Federation is currently disabled in the code, so this
  does nothing. In the future if set to `yes` this enables federation.
- `enableFileServer` - if set to `yes` the local file server is enabled
- `excludePluginList` - This is an array, list any plugins you want to be
  excluded from any wikis served in the array.
- `filePathRoot` - This is the root folder for the file server
- `fileURLPrefix` - This is the prefix used to access files on the file server
- `httpsPort` - This is the port to use for the https server
- `includePluginList` - This is an array of plugins that get added to every
  wiki served by the server.
- `namespacedWikis` - if this is set to `yes` wikis created have the name
  prefixed with the username of the person making the wiki. This prevents
  naming conflicts between users. So if the user named `inmysocks` makes a wiki
  called `foo`, the path to the wiki would be `inmysocks/foo` instead of `foo`.
- `perWikiFiles` - if this is set to `yes` files are saved in folders specific
  to each wiki instead of in a global files folder.
  - This means that to get the file `filename.jpg` for the wiki `foo` you would
    use `foo/files/filename.jpg`. Without this all files are in the same folder
    and accessed using `/files/filename.jpg` regardless of the wiki.
- `pluginsPath` - path to the plugins folder, either absolute or relative to
  `wikiPathBase`
- `reverseProxy` - if you are using a reverse proxy set this to `yes` so that
  the plugin library won't try and set CORS headers. I don't remember if this
  is still needed or not.
- `rootWikiName` - The name of the root wiki.
- `saltRounds` - the number of salt rounds used by bcrypt when hashing passwords
- `saveMediaOnServer` - if `yes` media is saved on the server and
  _canonical_uri tiddlers are made for media files, if `no` imported media gets
  embedded in tiddlers
- `serverKeyPath` - the path to the private key that goes with the server https
  certificate file.
- `serveWikiOnRoot` - if set to `yes` the index wiki gets served on `/` on the
  server, if `no` the `index.html` file gets served to work as a login page.
  - In the future this will be more configurable so you can easily make your
    own public-facing login page.
- `themesPath` - the path to the themes folder, either absolute or relative to
  `wikiPathBase`
- `tokenPrivateKeyPath` - the path to the private key to use for signing login
  tokens. This can be the same as the key used for the https server.
- `tokenTTL` - The length of time login tokens are valid for (get a reference
  for the format for this, the only thing I remember is you can do 12h for 12
  hours, and set whatever numebr you want)
- `useHTTPS`
- `wikiPathBase` - the folder that is used as the base for relative paths. It
  can be absolute or use on of the special
  values.
  - `cwd` uses the current working directory, which is where the script is run.
  - `home` uses the home folder of the person running the script
- `wikiPermissionsPath` - may not be used anymore since things are in the
  database
- `wikisPath` - the path to the wikis folder, either absolute or relative to
  `wikiPathBase`