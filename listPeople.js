const PouchDB = require('pouchdb')
const peopleDB = new PouchDB('db/people')

peopleDB.allDocs({include_docs: true}).then(function(result) {
  for (let i = 0; i < result.rows.length; i++) {
    console.log(result.rows[i])
  }
}).catch(function(err) {
  console.log(err)
})