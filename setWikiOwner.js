const PouchDB = require('pouchdb')
const settingsDB = new PouchDB('db/settings')

if (process.argv.length < 4) {
  console.log('Usage: node ./addperson.js wiki owner')
} else {
  const wiki = process.argv[2]
  const owner = process.argv[3]

  updateOwner(wiki, owner)
}

function updateOwner(wiki, owner) {
  // Check to make sure that the persons info doesn't already exist on the
  // server
  settingsDB.get('settings').catch(function(err) {
    throw err
  }).then(function(settingsObj) {
    settingsObj['wikis'][wiki]['__permissions']['owner'] = owner
    return settingsDB.put(settingsObj)
  }).catch(function(err) {
    throw err
  })
}