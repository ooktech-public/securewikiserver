#BIG NOTE OF DOOM!!!!

This is in beta. Not all functions are fully implemented, the documentation
isn't complete and there are administration functions that are not implemented.

Expect bugs and missing functionality.

# Secure Wiki Server

This is a server that can be used to serve TiddlyWiki online. It is made to
work with the Bob plugin.

For more in-depth information about the different sections look in the
Documentation folder.

The project is poorly organised and there are many old components around.

## Installation

### Linux and OSX

On linux or OSX the easiest way is to download and run `setup.sh`

This will check to make sure you have all the requirements:

- node and npm
- git
- openssl

if you don't have one or more of the requirements you will be prompted to
install them first.

As part of the installation the script will create an https certificate, put in
answers for each question, or just leave them blank and hit enter.

The script will ask if you want to start the server for setup, if you say yes
the server will start and automatically open a wiki in a browser.

The wiki that opens is the Admin wiki, it isn't finished yet and all that it
does is let you create an admin account.
Once this account is created only the admin account can open the Admin wiki.

For some reason after making the admin account newly created wikis aren't
showing up until the server is stopped and then restarted.
After a restart creating wikis works as expected.

See below for adding new accounts and logging in.

The server is started with nohup, which means that it becomes a background
process.

To stop the server use the script `stop.sh`, to start it again you can run `node index.js` to have output in the terminal, or start it as a background process using `start.sh`.

## Logging in

As part of the setup process the admin account gets created.
By default there is a login tab in the sidebar, to login click on the tab,
enter the username and password and then click `Login`.

If you create an account with the name and password both set to `Guest` and add
`guestLogin=yes` to the login widget like this:

```
<$login-widget guestLogin=yes/>
```

there will be a button labeled `Login as Guest` that can allow people without
accounts to login as a guest, at the moment this is just another login, so be
careful with it.

## Adding people

To add more people log in as the administartor and use the setpassword widget.

To use the setpassword widget put this in a tiddler:

```
<$setpassword-widget/>
```

then enter the name and password and click `Create Login` to add a new account.

After adding an account you can login with that account using the login widget.

Note: I haven't implemented a way to set the server access level of new
accounts yet, so only `Admin` accounts can add new people.


## Some notes

- Trying to open a wiki that doesn't exist, or that you don't have access too,
  will redirect you back to the index wiki.
- If there is a wiki/plugin/theme/etc that is not set to public and has no
  owner, Admin accounts have all permissions on the wiki. This is to ensure
  that there aren't lost wikis that no one has access to.
  - If an unlisted wiki is found in the wikis folder it is set to private with
    no owner, so they are accessible by the Admin account(s)
  - Before release I need to make this check to see if the listed owner actually
    has an account. If not the owner listing should be removed, then it reverts
    to being accessible by Admin accounts and making an account with the same
    name won't give people access to wikis they shouldn't have access to.

## Setup concerns

Each of these needs instructions and more explanation.

- You should never edit `Config.toml`, use `Local.toml`
- Make sure that the sub-modules are properly initialised and updated.
  - At the moment this is just tiddlywiki in the `TiddlyWiki5` folder
- Need to make an index wiki and correctly list the path to it.
  - The wiki needs to include the Bob plugin.
- Set the paths to the plugin, themes and editions folders (if any)
- Need to make sure that the .crt and private key files for the ssl part exist
  and the paths are listed correctly
- need to make sure that the key used to sign the tokens exists
  - Also the path to the key file. By default it is in `./.ssh/id_rsa`
    (relative to `~`)
- Need to setup accounts for people.
  - The command to add a person is `node addperson.js Name Password Level`
  - Guest gets set up with `node addperson.js Guest Guest Guest`
- In the wiki setup you have to set the `useExternalWSS` setting to true
- Make `~/Plugins`, `~/Themes` and `~/Editions` folders and put any plugins,
  themes and editions you want to have accessible to the Bob server in them.
  - The folders should be the same as they are in the tiddlywiki core, so
    plugins and themes are in folders like `Author/PluginName` and editions are
    in folders that are named the same as the edition.

## What it does

There is an https server that can serve pages. It accepts POSTs to
`/authenticate` with the `name` and `pwd` in the POST body. These are checked
against what is listed in the `/people` folder. If the credentials match than
a signed json web token is returned as part of response to the https request.
This token is then sent with every websocket message to the websocket server to
have authenticated communication with the server.

## Setup instructions

If you just want the commands to type in in order they are after this list.

- Make sure you have git, npm and node installed (which version is needed? I
  don't know. Newest one and LTS versions work.)
- Clone the repo onto your server
  (`git clone git@gitlab.com:ooktech/SecureWikiServer.git`)
  - initialise the TiddlyWiki5 submodule
    (`git submodule init && git submodule update`)
- `npm install` to install all the dependencies
- Make sure you have the certificate files on the server (instructions to make
  your own self signed certs here: https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs) For testing see the command in the last section below.
- Update `certPath` and `serverKeyPath` in the `Local.toml` file in the
  `Config` folder to point to the certificate and key files.
  - You will have to create the `Local.toml` file.
  - Never edit `Config.toml`, settings in `Local.toml` override `Config.toml`
    so just make the same entry in `Local.toml` if you want to change a default
    setting.
- If needed generate a public-private key pair for token signing
- Update the `tokenPrivateKeyPath` to point to your private key. It just
  occurred to me that you can use the same key as the signing key for the
  certificate. There are security reasons that you would want separate keys.
- If you want you can change the `httpsPort` value also. If you have root
  access you can make this `443` and run the server as root. Otherwise it must
  be greater than `1024`
- set plugin folder path in `Local.toml`
- Get needed plugins (`git clone git@github.com:OokTech/TW5-Bob.git Bob`)
- Make wiki (`node tiddlywiki.js IndexWiki --init empty`)
- Add Bob to the list of plugins in the wiki
- Run `./start.sh`
- Go to the url of your server on the needed port. So if your server is
  `www.example.com` and `httpsPort` is left as the default `8443`, then you go
  to `www.example.com:8443`.
- To stop the wiki server run `./stop.sh`

### All the command line instructions

```
git clone git@gitlab.com:ooktech/SecureWikiServer.git
cd SecureWikiServer
git submodule init && git submodule update
npm install
openssl req -newkey rsa:2048 -nodes -keyout private.key -x509 -days 365 -out certificate.crt
mkdir Plugins
mkdir Themes
mkdir Wikis
cd Plugins
mkdir OokTech
cd OokTech
git clone git@github.com:OokTech/TW5-Bob.git Bob
cd ../..
node ./TiddlyWiki5/tiddlywiki.js ./Wikis/IndexWiki empty
```

the `tiddlywiki.info` file in the IndexWiki needs to have Bob listed in its
plugins, so you need to open a text editor and edit the file to add it.

Once that is done start the server with

```
./start.sh
```

## Components

- An express server (but a normal node http(s) server would work also.)
- The ws node module for the websockets
- The jsonwebtoken module for the JWT part (login authentication)
- The bcrypt module for storing hashed passwords

## Some explanation

- The express server is well developed and has many modules and is very
  extensible so we used it here.
- Never store passwords. The bcrypt module takes care of that for us by storing
  appropriately salted hashes of the passwords instead of the passwords them
  selves. See the docs for the bcrypt library for more about that.
- Authentication for https traffic is pretty standard, but https authentication
  doesn't apply to the websockets channel. So using the https channel you
  authenticate and then get a signed token from the server. Then you send this
  token along with all the websocket messages over the wss channel. Then the
  server can just verify the token.
- The tokens have the authentication level listed in the token so you can have
  different levels of authentication.

## Some notes to expand upon

- The websocket connections are saved in the connections object. To include it
  in a script use `var connections = require('./js/websocketserver').connections`. Each element in the connections
  object has the properties `socket` which is the connection object for sending
  messages, and `active` which is a boolean value indicating if the connection is active or not.

  `connection[index].socket.send(message)`

  Messages send should be stringified json objects.

- Setting up SSL and self signed certificates or other certificates. ref: https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs
- Also this https://blog.cloudboost.io/everything-about-creating-an-https-server-using-node-js-2fc5c48a8d4e
- This for forcing https https://stackoverflow.com/questions/11744975/enabling-https-on-express-js

## Creating the private key and certificate

This is for self-signed certificates and https. Use at your own risk, it may
make everything explode and give everyone in the world access to your computer.

- `openssl req -newkey rsa:2048 -nodes -keyout domain.key -x509 -days 365 -out domain.crt`
