/*
 * Copyright (C) OokTech LLC 2017
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Written by Jedediah and Joshua Carty <ook@ooktech.com>
 *
 * Description: Determines which configuration and then exposes it as an object,
 * acting as the initial state through the Status module. Configurations are
 * created by a tool but can be hand edited if need be. Note that the parser is
 * extremely strict as to what is a valid JSON file (beware extra commas!).
 */

const PouchDB = require('pouchdb')
const settingsDB = new PouchDB('db/settings')

var settingsObj = {}

/*
  Inputs:

  setting - the new setting(s) to save, in json format with the same structure
    as the saved settings use.
  local - unused here, it is kept for compatibility with the non-db based
    settings
  overwrite - If this is set to true than the 'setting' input is used as the
    complete settings and the existing settings are all discarded in favour of
    using the input.
*/
settingsObj.saveSetting = function (setting, local, overwrite) {
  if (overwrite) {
    settingsObj.settings = setting
  } else {
    updateConfig(settingsObj.settings, setting)
  }
  return settingsDB.get('settings')
  .then(function(oldSettingsDoc) {
    updateConfig(oldSettingsDoc, setting)
    return settingsDB.put(oldSettingsDoc)
  })
  .then(function(result) {
    return settingsDB.get('settings')
  })
  .then(function(result) {
    settingsObj.settings = result
    return result
  })
  .catch(function(err) {
    console.log(err)
  })
}

/*

  Inputs:

  main - unused, only here for backwards compatibility
  local - unused, only here for backwards compatibility

*/
settingsObj.loadConfig = function (){
   return settingsDB.get('settings').then(function(settingsDoc) {
    settingsObj.settings = settingsDoc
    return settingsDoc
  }).catch(function(err) {
    console.log(err)
  })
}

/*
  given a local and a global config, this returns the global config but with
  any properties that are also in the local config changed to the values given
  in the local config.
  Changes to the configuration are later saved to the local config.
*/
const updateConfig = function (globalConfig, localConfig) {
  // Walk though the properties in the localConfig, for each property set the
  // global config equal to it, but only for singleton properties. Don't set
  // something like GlobalConfig.Accelerometer = localConfig.Accelerometer, set
  // globalConfig.Accelerometer.Controller =
  // localConfig.Accelerometer.Contorller
  Object.keys(localConfig).forEach(function (key, index) {
    if (typeof localConfig[key] === 'object' && !(localConfig[key] instanceof Array)) {
      if (!globalConfig[key]) {
        globalConfig[key] = {}
      }
      // do this again!
      updateConfig(globalConfig[key], localConfig[key])
    } else {
      globalConfig[key] = localConfig[key]
    }
  })
}


settingsObj.settings = {}
settingsObj.settingsDB = settingsDB

module.exports = settingsObj