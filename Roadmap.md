# SecureWiki Roadmap

- Per user settings that are persistent across wikis, saved as part of the login
- make a wiki that is designed just for configuration tasks
- make an indicator to show if you are logged in or not