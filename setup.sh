#!/usr/bin/env bash


if command -v git ; then
  GIT=true
else
  echo >&2 "git is required but it's not installed."
  GIT=false
fi

if command -v node ; then
  NODE=true
else
  echo >&2 "node is required but it's not installed."
  NODE=false
fi

if command -v npm ; then
  NPM=true
else
  echo >&2 "npm is required but it's not installed."
  NPM=false
fi

if command -v openssl ; then
  OPENSSL=true
else
  echo >&2 "openssl is required but it's not installed."
  OPENSSL=false
fi

if [ "$GIT" = "true" ] && [ "$NODE" = "true" ] && [ "$NPM" = "true" ] && [ "$OPENSSL" = "true" ]; then
  #git clone git@gitlab.com:ooktech/SecureWikiServer.git
  git clone https://gitlab.com/ooktech-public/securewikiserver.git SecureWikiServer
  cd SecureWikiServer
  #git checkout new
  git submodule init
  git submodule update
  npm install
  openssl req -newkey rsa:2048 -nodes -keyout private.key -x509 -days 365 -out certificate.crt
  mkdir Plugins
  mkdir Themes
  mkdir Wikis
  mkdir db
  cd Plugins
  mkdir OokTech
  cd OokTech
  git clone https://github.com/OokTech/TW5-Bob.git Bob
  git clone https://github.com/OokTech/TW5-Login.git Login
  cd ../..
  cd SecureWikiServer
  cp -r ./DefaultEdition ./IndexWiki
  cp -r ./AdminEdition ./Wikis/Admin
  node ./InitialiseDatabase.js

  read -p "Start server for setup? (y/n): " startServer
  if [ "$startServer" = "y" ]; then
    ./start.sh -y
  else
    echo >&2 "type cd SecureWikiServer && node index.js to start the server for testing."
    echo >&2 "type cd SecureWikiServer && ./start.sh to start the server in the background."
    echo >&2 "Remember that you have to type https:// before the URL if you are using the https server."
  fi
else
  echo >&2 "install necessary dependencies listed above then run the script again."
  exit 1
fi
