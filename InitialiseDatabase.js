const PouchDB = require('pouchdb')
const settingsDB = new PouchDB('./db/settings')

const defaultSettings = {
  _id: "settings",
  certPath:"./certificate.crt",
  disableBrowserAlerts: "no",
  disableFileWatchers: "no",
  editionsPath:"./Editions",
  enableFederation: "no",
  enableFileServer: "no",
  excludePluginList: [],
  filePathRoot: "./files",
  fileURLPrefix: "files",
  httpsPort:"8443",
  includePluginList: [],
  namespacedWikis: "false",
  persistentUsernames: "no",
  perWikiFiles: "no",
  pluginsPath:"./Plugins",
  reverseProxy:"no",
  rootWikiName:"IndexWiki",
  saltRounds:"10",
  saveMediaOnServer: "yes",
  serverKeyPath:"./private.key",
  serveWikiOnRoot:"yes",
  themesPath:"./Themes",
  tokenPrivateKeyPath:"./private.key",
  tokenTTL:"24h",
  useHTTPS:"yes",
  wikiPathBase:"cwd",
  wikiPermissionsPath:"./Config/wikiPermissions.toml",
  wikisPath:"./Wikis",
  actions: {
    view:["browserTiddlerList", "test", "ping", "pong", "ack", "getViewableWikiList", "findAvailableWikis", "getSkinnyTiddlers", "getFullTiddler", "downloadHTMLFile"],
    edit:["browserTiddlerList", "saveTiddler", "cancelEditingTiddler", "clearStatus", "deleteTiddler", "editingTiddler", "ack", "syncChanges", "internalFetch", "getPluginList", "setLoggedIn", "setConfigurationInterface", "findAvailableWikis", "getThemeList", "getSkinnyTiddlers", "getFullTiddler"],
    admin:["browserTiddlerList", "addWiki", "saveSettings", "shutdownServer", "updateRoutes", "buildHTMLWiki", "newWikiFromTiddlers", "createNewWiki", "restartServer", "unloadWiki", "ack", "savePluginFolder", "getPluginList", "getViewableWikiList", "updateTiddlyWikiInfo", "setLoggedIn", "setConfigurationInterface", "findAvailableWikis", "internalFetch", "duplicateWiki", "setWikiPermissions", "getThemeList", "getGitPlugin", "renameWiki", "deleteWiki", "listFiles", "mediaScan", "getSkinnyTiddlers", "getFullTiddler"],
    scripts:["runScript", "stopScripts"],
    owner:["browserTiddlerList", "test", "ping", "pong", "saveTiddler", "cancelEditingTiddler", "clearStatus", "deleteTiddler", "editingTiddler", "unloadWiki", "ack", "internalFetch", "syncChanges", "getPluginList", "getViewableWikiList", "updateTiddlyWikiInfo", "setLoggedIn", "setConfigurationInterface", "setWikiPermissions", "findAvailableWikis", "getThemeList", "getGitPlugin", "makeImagesExternal", "renameWiki", "deleteWiki", "listFiles", "mediaScan", "getSkinnyTiddlers", "getFullTiddler", "updateRoutes"],
    upload:[],
    push:[],
    fetch:[]
  },
  API: {
    enableDelete: "no",
    enableCreate: "no",
    enableFetch:"no",
    enablePush:"no",
    pluginLibrary:"no",
    updatePasswords:"yes",
    fetchEndpoint:"/api/fetch",
    pushEndpoint:"/api/push"
  },
  admin: {
    Normal:["viewSettings"],
    Admin:["pushPlugins", "viewSettings", "editSettings", "addPerson", "updateSetting", "saveSettings", "savePluginFolder", "makeImagesExternal", "deleteWiki", "newWikiFromTiddlers", "createNewWiki", "duplicateWiki"]
  },
  wikis: {
    RootWiki:{
      __path: 'IndexWiki',
      __permissions: {
        owner:false,
        public:'yes',
        editors:[],
        viewers:[],
        fetchers:[],
        pushers:[],
        access: {
          Guest:["view"],
          Normal:["view", "edit"],
          Admin:["view", "edit", "admin", "upload"]
        }
      }
    },
    Admin: {
      __path: 'Admin',
      __permissions: {
        owner: false,
        public: 'yes',
        editors:[],
        viewers:[],
        fetchers:[],
        pushers:[],
        access: {
          Guest:["view"],
          Normal:["view", "edit"],
          Admin:["view", "edit", "admin", "upload"]
        }
      }
    }
  }
}

settingsDB.get("settings").then(function(thisDoc) {
  console.log('settings database exists')
  /*
  defaultSettings._rev = thisDoc._rev
  settingsDB.put(defaultSettings).then(function(response) {
    console.log('Initialised database')
  }).catch(function(err) {
    throw err
  })
  */
}).catch(function (err) {
  if (err.name === 'not_found') {
    settingsDB.put(defaultSettings).then(function(response) {
      console.log('Initialised database')
    }).catch(function(err) {
      throw err
    })
  } else {
    throw err
  }
})
