const PouchDB = require('pouchdb')
const settingsDB = new PouchDB('./db/settings')

settingsDB.get('settings').then(function(result) {
  console.log(JSON.stringify(result, null, 2))
}).catch(function(err) {
  console.log(err)
})