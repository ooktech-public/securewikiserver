/*
  This is a generic extensible websocket server.

  Each message comes in as a json object. Each message has to have a
  type property which names the message handler function to use.

  The handler function is passed the message as a json object. Any
  authentication or validation is done by the message handlers.

  Things this shows:
  - Setting up a websocket server
  - Exporting things as a module
*/

// Require needed libraries
const ws = require('ws')
const path = require('path')
const jwt = require('jsonwebtoken')
const url = require('url')
const wiki = require('../wikiStart.js').wiki
const checkPermissions = require('./checkPermissions.js')
const messageHandlers = require('./websocketmessagehandlers.js')

// Initialise variables
const websocketserver = {}
const connections = []

/*
  This is the init function. It uses an existing server. Any upgrade requests
  made to the existing server are passed to the websockets server by adding the
  server.on('upgrade', ...)  handler.
*/
const init = function (server) {
  // This lets you create the websocket server using a port number or an
  // existing http(s) server. To use secure websockets you need to use an
  // existing https server, otherwise it doesn't make much difference.
  const WebSocketServer = ws.Server
  const wss = new WebSocketServer({noServer: true})
  const federationWss = new WebSocketServer({noServer: true});
  // This sets the handler function for the 'connection' event. This fires
  // every time a new connection is initially established.
  wss.on('connection', handleConnection)
  // Make it so websocket connections are handled by the websocket server, not
  // the normal https server.
  server.on('upgrade', function(request, socket, head) {
    const pathname = url.parse(request.url).pathname
    if (pathname === '/api/federation/socket') {
      console.log('YOU NEED TO IMPLEMENT THE FEDERATION SOCKET!!')
    } else {
      wss.handleUpgrade(request, socket, head, function(ws) {
        wss.emit('connection', ws, request)
      })
    }
  })
  wiki.tw.PruneTimeout = setInterval(function(){
        wiki.tw.Bob.PruneConnections();
      }, 10000);
}

// This function sets up how the websocket server handles incomming
// connections. It is generic and extensible so you can use this same server
// to make many different things.
function handleConnection (client) {
  console.log('New Connection')
  connections.push({'socket':client, 'active': true})
  client.on('message', handleMessage)
  connections[Object.keys(connections).length-1].index = (Object.keys(connections).length-1)
  //Maybe send the list tiddlers message here
  const message = {type: 'pong'}
  wiki.tw.Bob.SendToBrowser(connections[Object.keys(connections).length-1], message);
}

  // This function sets up how the websocket server handles incomming
  // messages. It is generic and extensible so you can use this same server
  // to make many different things.
function handleMessage(event) {
  // This imports the handlers for the example chat application.
  const self = this
  // Determine which connection the message came from
  const thisIndex = connections.findIndex(function(connection) {return connection.socket === self})
  try {
    const eventData = JSON.parse(event)
    // Add the source to the eventData object so it can be used later.
    eventData.source_connection = thisIndex
    if (eventData.wiki && eventData.wiki !== connections[thisIndex].wiki && !connections[thisIndex].wiki) {
      connections[thisIndex].wiki = eventData.wiki;
      // Make sure that the new connection has the correct list of tiddlers
      // being edited.
      wiki.tw.Bob.UpdateEditingTiddlers(false, eventData.wiki);
    }
    // This may or may not be required to prevent a security problem, I don't know.
    if (eventData.wiki === connections[thisIndex].wiki) {
      // Make sure we have a handler for the message type
      if (typeof messageHandlers[eventData.type] === 'function') {
        // Check authorisation
        const key = require('./loadSecrets.js')
        let decoded = false
        if (eventData.token && eventData.type !== 'ack') {
          try {
            decoded = jwt.verify(eventData.token, key)
          } catch (e) {
            decoded = false
          }
        }
        const authorised = checkPermissions(eventData.wiki, {decoded: decoded}, eventData.type)
        if (authorised) {
          eventData.decoded = decoded
          messageHandlers[eventData.type](eventData)
        }
        // If unauthorised just ignore it.
      } else {
        console.log('No handler for message of type ', eventData.type)
      }
    } else {
      console.log('Wiki listed in message and connected wiki don\'t match')
    }
  } catch (e) {
    if (e.name === 'TokenExpiredError') {
      console.log('token expired', e)
    } else {
      console.log("WebSocket error: ", e)
    }
  }
}

// This module exports an empty object at the top level.
// It also exports the init function to create and start the server.
// It also exports the connections object which is used to send messages to
// connected devices.
module.exports = websocketserver
module.exports.init = init
module.exports.connections = connections
module.exports.handleMessage = handleMessage
