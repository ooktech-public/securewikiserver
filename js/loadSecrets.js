const settings = require('../LoadConfig.js').settings
const fs = require('fs')
const path = require('path')

// This doesn't get a try block, if it fails it should crash.
let baseDir = process.pkg?path.dirname(process.argv[0]):process.cwd();
if (settings.wikiPathBase === 'homedir') {
  baseDir = os.homedir();
} else if (settings.wikiPathBase === 'cwd' || !settings.wikiPathBase) {
  baseDir = process.pkg?path.dirname(process.argv[0]):process.cwd();
} else {
  baseDir = path.resolve(settings.wikiPathBase);
}
const key = fs.readFileSync(path.resolve(baseDir, settings.tokenPrivateKeyPath))

module.exports = key
