const path = require('path')
const os = require('os')
const settings = require('../LoadConfig.js').settings
const wikiPermissions = settings.wikis || {}
const pluginPermissions = settings.plugins || {}
const themePermissions = settings.themes || {}
const editionPermissions = settings.editions || {}
const languagePermissions = settings.languages || {}

/*
  This is a generic function to check if a person has a permission on a wiki
  based on the token they sent.

  We need to modify this so that it creates a list of what the token allows and
  then checks to see if the permission is in that list. The logic here is just
  complex and asking for bugs.

  response.decode.level -> this gives what server permissions are allowed and
    what group the person belongs to for permissions given to groups on the
    wiki.

  fullName - the name of the message/wiki/plugin/theme/language/edition in
    question
  response - the server response object
  permission - the specific permission requested (view, edit, etc.)
  type - the type of thing being checked
    - message
    - wiki
    - plugin
    - theme
    - edition

    There are server messages and wiki messages.
      - Server Messages are things like add users and create wikis
      - Wiki Messages are messages that have to do with either viewing or
        editing wikis.

    Wiki permissions are about managing wikis, like changing permissions

    for plugin, theme and edition the there permissions are
      - view - view lets you use the plugin in a wiki, or fetch it using the
          api
      - upload - upload lets you upload new plugins to the server that do not
          already exist
      - edit - edit lets you edit or overwrite an existing plugin on the
          server.
*/
function checkPermission (fullName, response, permission, type) {
  // Quick checks to filter out errors and weirdness
  if(!response) {
    return false
  }

  // The status route is always available, what it shows is what changes and
  // that is handled elsewhere.
  if (permission === 'ack' || permission === 'setLoggedIn') {
    return true
  }

  // This is a special case that allows the administartor account to be created
  // when the server is being set up.
  if (permission === 'addPerson' && !response.hasAdmin) {
    return true
  }

  fullName = fullName || ''
  const nameParts = fullName.split('/')
  let permissionsObject = {};


  if (type === 'plugin') {
    settings.pluginLibrary = settings.pluginLibrary || {}
    if (settings.pluginLibrary.allPublic === 'yes' && permission === 'view') {
      return true
    }
    permissionsObject = pluginPermissions
  } else if (type === 'edition') {
    settings.editionLibrary = settings.editionLibrary || {}
    if (settings.editionLibrary.allPublic === 'yes' && permission === 'view') {
      return true
    }
    permissionsObject = editionPermissions
  } else if (type === 'theme') {
    settings.themeLibrary = settings.themeLibrary || {}
    if (settings.themeLibrary.allPublic === 'yes' && permission === 'view') {
      return true
    }
    permissionsObject = themePermissions
  } else if (type === 'language') {
    settings.languageLibrary = settings.languageLibrary || {}
    if (settings.languageLibrary.allPublic === 'yes' && permission === 'view') {
      return true
    }
    permissionsObject = languagePermissions
  } else {
    permissionsObject = wikiPermissions
  }
  nameParts.forEach(function(part) {
    permissionsObject = permissionsObject[part] || {}
  })
  permissionsObject = permissionsObject.__permissions || {}

  // make sure that all the required things exist
  response = response || {}
  response.decoded = response.decoded || {}
  response.decoded.level = response.decoded.level || 'Guest'

  permissionsObject.access = permissionsObject.access || {}
  permissionsObject.access[response.decoded.level] = permissionsObject.access[response.decoded.level] || []
  permissionsObject.viewers = permissionsObject.viewers || []
  permissionsObject.editors = permissionsObject.editors || []
  permissionsObject.fetchers = permissionsObject.fetchers || []
  permissionsObject.pushers = permissionsObject.pushers || []

  if(!response.decoded) {
    return false
  }

  // Special case for wikis/themes/plugins/etc that have no owners and aren't
  // public, Admin accounts have all permissions on them to ensure that they
  // can have permissions set.
  if ((!permissionsObject.public || permissionsObject.pubilc === 'no') && (!permissionsObject.owner) && response.decoded.level === 'Admin') {
    return true
  }

  // Anyone can view public wikis
  if (permissionsObject.public === 'yes' && (permission === 'view' || settings.actions.view.indexOf(permission) !== -1)) {
    return true
  }

  // This has to be after the check for public wikis or it will prevent anyone
  // from accessing public wikis
  if (Object.keys(response.decoded).length === 0 || typeof response.decoded.name === 'undefined' || response.decoded.name.trim() === '') {
    return false
  }

  // Start with an empty permissions list, then for each category check if the
  // person has it either by server access level or explicitly for the wiki, if
  // so than add the permissions for that category to the list of permissions
  // the person has.
  let userPermissions = []
  // Wiki level permissions - these are set per wiki in the wiki permissions
  if (permissionsObject.owner === response.decoded.name) {
    if (settings.restrictPushFetch === 'yes') {
      userPermissions = ['view', 'edit', 'owner']
    } else {
      userPermissions = ['fetch', 'push', 'view', 'edit', 'owner']
    }
  } else {
    if (permissionsObject.viewers.indexOf(response.decoded.name) > -1 || permissionsObject.access[response.decoded.level].indexOf('view') > -1) {
      userPermissions.push('view')
    }
    if (permissionsObject.editors.indexOf(response.decoded.name) > -1 || permissionsObject.access[response.decoded.level].indexOf('edit')) {
      userPermissions.push('edit')
    }
    if (permissionsObject.fetchers.indexOf(response.decoded.name) > -1) {
      userPermissions.push('fetch')
    }
    if (permissionsObject.pushers.indexOf(response.decoded.name) > -1) {
      userPermissions.push('push')
    }
  }
  // Server level permissions - a person has these on the whole server. They
  // are set by the login level.
  userPermissions.push(response.decoded.level)
  // Now check if the action or permission is listed.
  if (userPermissions.indexOf(permission) > -1) {
    return true
  }
  // This checks message permissions
  for (let i = 0; i < userPermissions.length; i++) {
    let thisOne = userPermissions[i]
    if (settings.actions[thisOne]) {
      if (settings.actions[thisOne].indexOf(permission) > -1) {
        return true
      }
    }
    if (settings.admin[thisOne]) {
      if (settings.admin[thisOne].indexOf(permission) > -1) {
        return true
      }
    }
  }

  return false
}

module.exports = checkPermission
module.exports.wikiPermissions = wikiPermissions
