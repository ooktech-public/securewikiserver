/*
  These are the websocket message handler functions for the simple chat
  application.

  Things this demonstrates:
  - using token based authentication for websocket messages
  - verifying jsonwebtokens using a pivate key
  - sending messages from node using websockets
*/

const PouchDB = require('pouchdb')
const settingsDB = require('../LoadConfig.js').settingsDB
const messageHandlers = {}

/*
  This is for adding a login for a new person

  The authentication for this is done before this funciton is called.
*/
messageHandlers.addPerson = function (data) {
  if (data.name && data.password && data.level) {
    return require('../addperson.js').saveCredentials(data.name, data.password, data.level)
  }
}

/*
  This sets a wikis owner.

  Inputs are
  - newOwner - the login name of the new owner
  - wikiName - the full wiki name
*/
messageHandlers.setWikiOwner = function (data) {
  if (data.newOwner && data.wikiName) {
    return settingsDB.get('settings')
    .catch(function(err) {
      throw err
    })
    .then(function(settingsObj) {
      settingsObj['wikis'][data.wikiName]['__permissions']['owner'] = data.newOwner
      return settingsDB.put(settingsObj)
    })
    .catch(function(err) {
      throw err
    })
  }
}

/*

*/
messageHandlers.addViewers = function (data) {
  if (data.newViewers && data.wikiName) {
    if (Array.isArray(viewers)) {
      return settingsDB.get('settings').then(function(result) {
        const nameParts = data.wikiName.split('/')
        let wikiPermissionsObject = result.wikiPermissions
        nameParts.forEach(function(thisPart) {
          wikiPermissionsObject = wikiPermissionsObject[thisPart]
        })
        wikiPermissionsObject.__permissions.viewers = wikiPermissionsObject.__permissions.viewers.concat(data.newViwers)
        return settingsDB.put(settings)
      }).catch(function(err) {
        console.log(err)
      })
    }
  }
}

/*

*/
messageHandlers.addEditors = function (data) {
  if (data.newEditors && data.wikiName) {
    if (Array.isArray(viewers)) {
      return settingsDB.get('settings').then(function(result) {
        const nameParts = data.wikiName.split('/')
        let wikiPermissionsObject = result.wikiPermissions
        nameParts.forEach(function(thisPart) {
          wikiPermissionsObject = wikiPermissionsObject[thisPart]
        })
        wikiPermissionsObject.__permissions.editors = wikiPermissionsObject.__permissions.editors.concat(data.newEditors)
        return settingsDB.put(settings)
      }).catch(function(err) {
        console.log(err)
      })
    }
  }
}

/*

*/
messageHandlers.addFetchers = function (data) {
  if (data.newFetchers && data.wikiName) {
    if (Array.isArray(viewers)) {
      return settingsDB.get('settings').then(function(result) {
        const nameParts = data.wikiName.split('/')
        let wikiPermissionsObject = result.wikiPermissions
        nameParts.forEach(function(thisPart) {
          wikiPermissionsObject = wikiPermissionsObject[thisPart]
        })
        wikiPermissionsObject.__permissions.fetchers = wikiPermissionsObject.__permissions.fetchers.concat(data.newFetchers)
        return settingsDB.put(settings)
      }).catch(function(err) {
        console.log(err)
      })
    }
  }
}

/*

*/
messageHandlers.addPushers = function (data) {
  if (data.newPushers && data.wikiName) {
    if (Array.isArray(viewers)) {
      return settingsDB.get('settings').then(function(result) {
        const nameParts = data.wikiName.split('/')
        let wikiPermissionsObject = result.wikiPermissions
        nameParts.forEach(function(thisPart) {
          wikiPermissionsObject = wikiPermissionsObject[thisPart]
        })
        wikiPermissionsObject.__permissions.pushers = wikiPermissionsObject.__permissions.pushers.concat(data.newPushers)
        return settingsDB.put(settings)
      }).catch(function(err) {
        console.log(err)
      })
    }
  }
}

/*
  Set the public status of a wiki, either true or false
*/
messageHandlers.setPublic = function (data) {
  if (typeof data.public !== 'undefined' && data.wikiName) {
    return settingsDB.get('settings').then(function(result) {
      const nameParts = data.wikiName.split('/')
      let wikiPermissionsObject = result.wikis//result.wikiPermissions.wikis
      nameParts.forEach(function(thisPart) {
        wikiPermissionsObject = wikiPermissionsObject[thisPart]
      })
      wikiPermissionsObject.__permissions.public = data.public
      return settingsDB.put(result)
    }).catch(function(err) {
      console.log(err)
    })
  }
}

module.exports = messageHandlers