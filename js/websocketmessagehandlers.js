/*
  These are the websocket message handler functions for the simple chat
  application.

  Things this demonstrates:
  - using token based authentication for websocket messages
  - verifying jsonwebtokens using a pivate key
  - sending messages from node using websockets
*/

const messageHandlers = {}

// A function to add message handlers.
const addHandlers = function (handlers) {
  // We expect an object where each property is a function for a different
  // message handler.
  if (typeof handlers === 'object') {
    for (handler in handlers) {
      if (typeof handlers[handler] === 'function') {
        messageHandlers[handler] = handlers[handler]
      }
    }
  }
}

module.exports = messageHandlers
module.exports.addHandlers = addHandlers
