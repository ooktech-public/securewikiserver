#!/bin/bash

# This starts the node server using nohup so it stays up until the server is
# restarted or it is manually stopped using stop.sh
# The process id is stored in wikiserver.pid so that the process can be stopped
# without trouble with stop.sh
source ../.bashrc

if [ ! -f wikiserver.pid ]; then
  echo '' > wikiserver.pid
fi

OLDPID=$(cat wikiserver.pid)

# Check to make sure that the process isn't already running before trying to
# start it
if [ -n "$(ps -p $OLDPID -o pid=)" ]; then
  echo "Server is already running with PID $OLDPID"
  echo "Remember that you have to type https:// before the URL if you are using the https server."
else
  nohup node ./index.js &

  PID=$!

  echo $PID > wikiserver.pid

  echo "Server is running with PID $PID"

  if [ "$1" = "-y" ] && [ -z "$2" ]; then
    echo "The wiki will open automatically in a moment."
    sleep 5
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
      xdg-open https://localhost:8443
    elif [[ "$OSTYPE" == "darwin"* ]]; then
      open https://localhost:8443
    fi
  fi
  if [ "$1" = "-y" ] && [ ! -z "$2" ]; then
    echo "The wiki will open automatically in a moment."
    sleep 5
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
      xdg-open https://localhost:$2
    elif [[ "$OSTYPE" == "darwin"* ]]; then
      open https://localhost:$2
    fi
  fi
  if [ ! "$1" = "-y" ]; then
    echo "Remember that you have to type https:// before the URL if you are using the https server."
  fi
fi
