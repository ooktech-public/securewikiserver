/*
  This is the http server component for initial authentication that can be used
  by other systems. It was made to work with a websocket server where each
  message is sent with a token.

  The basic web socket server is ./js/websocketserver.js and is made to be
  reusable by allowing you to add message handlers.
  Example message handlers are in ./js/websocketmessagehandlers.js

  It creates the people folder in the same place as this script.
  The people folder.

  Things this shows:
  - Setting up a simple express server
  - Creating an authentication route
  - Setting up an https server using a private key and certificate
*/

const fs = require('fs')
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const cookieParser = require('cookie-parser')
const PouchDB = require('pouchdb')
const peopleDB = new PouchDB('db/people')
let settings = {}
let hasAdmin = false;

function getBasePath() {
  const currPath = path.parse(process.argv[0]).name !== 'node' ? path.dirname(process.argv[0]) : process.cwd();
  let basePath = '';
  settings.wikiPathBase = settings.wikiPathBase || 'cwd';
  if(settings.wikiPathBase === 'homedir') {
    basePath = os.homedir();
  } else if(settings.wikiPathBase === 'cwd' || !settings.wikiPathBase) {
    basePath = path.parse(process.argv[0]).name !== 'node' ? path.dirname(process.argv[0]) : process.cwd();
  } else {
    basePath = path.resolve(currPath, settings.wikiPathBase);
  }
  return basePath;
}

require('./LoadConfig.js').loadConfig().then(function(loadedSettings) {
  settings = loadedSettings;
  return peopleDB.allDocs({include_docs: true})
})
.then(function(result) {
  for (let i = 0; i < result.rows.length; i++) {
    if (result.rows[i].doc.level === 'Admin') {
      hasAdmin = true;
      break
    }
  }
  settings.hasAdmin = settings.hasAdmin || hasAdmin
  return settings
})
.then(function(result) {
  run(result)
})
.catch(function(err) {
  console.log(err)
})

function run(settings) {
  if (settings.useHTTPS === 'yes') {
    var https = require('https')
  } else {
    var http = require('http')
  }

  //wiki.baseDir = getBasePath();
  const wiki = require('./wikiStart.js')(settings, getBasePath());
  const app = express()

  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({extended:true}))
  app.use(cookieParser())

  app.post('/authenticate', function (req, res) {
    // Get the authentication heanders and stuff
    // Check to make sure the header send a name and password
    if (req.body.name && req.body.pwd) {
      // Set headers
      res.setHeader('Access-Control-Allow-Origin', '*')
      try {
        // Get the stored hash
        peopleDB.get(req.body.name).then(function(data) {
          if (data.hash && data.level) {
            // Compare the password to the stored hash
            const goodPassword = bcrypt.compareSync(req.body.pwd, data.hash)
            if (goodPassword) {
              // Check the headers against the username and password
              // Create the token for it
              // Sign the token using the rsa private key of the server
              const key = require('./js/loadSecrets.js')
              const token = jwt.sign({level: data.level, name: req.body.name}, key, {expiresIn: settings.tokenTTL})
              res.status(200)
              // Send the token back
              return res.send(token)
            } else {
              return res.status(403).send(false)
            }
          } else {
            return res.status(403).send(false)
          }
        }).catch(function(err) {
          console.log('Database error:',err)
          res.status(400).send(false)
        })
      } catch (e) {
        console.log(`Could not authenticate ${req.body.name}`)
        res.status(400).send(false)
      }
    } else {
      res.status(400).send(false)
    }
  })

  /*
    This checks to see if there is a valid token with the request
    or that the wiki in question is set to public
  */
  const checkAuthentication = function(req, res, next) {
    req.hasAdmin = settings.hasAdmin
    if (settings.hasAdmin === false && req.originalUrl !== '/Admin' && !req.originalUrl.startsWith('/api/')) {
      res.hasAdmin = false
      res.redirect('/Admin')
      return
    } else if (settings.hasAdmin === false && req.originalUrl === '/Admin') {
      res.hasAdmin = false
      return next()
    }
    const wikiPermissions = settings
    const regexp = new RegExp(`^${req.baseUrl}\/?`)
    let wikiName = req.originalUrl.replace(regexp, '')
    if (wikiName === '') {
      wikiName = 'RootWiki'
    }
    // check if the wiki is public first
    wikiPermissions.wikis = wikiPermissions.wikis || {}
    wikiPermissions.wikis[wikiName] = wikiPermissions.wikis[wikiName] || {}
    wikiPermissions.wikis[wikiName].__permissions = wikiPermissions.wikis[wikiName].__permissions || {}
    if (wikiPermissions.wikis[wikiName].__permissions.public === 'yes' || req.originalUrl.startsWith('/api/')) {
      const key = require('./js/loadSecrets.js')
      // Add the decoded token to res object.
      if (req.cookies) {
        if (req.cookies.token) {
          try {
            res.decoded = jwt.verify(req.cookies.token, key)
          } catch (e) {
            res.decoded = false
          }
        }
      }
      return next()
    } else {
      // If the wiki isn't public than check if there is a valid token
      try {
        const key = require('./js/loadSecrets.js')
        const decoded = jwt.verify(req.cookies.token, key)
        if (decoded) {
          // Add the decoded token to res object.
          res.decoded = decoded
          return next()
        } else {
          res.redirect('/')
        }
      } catch (e) {
        res.redirect('/')
      }
    }
  }

  /*
    Setup the wiki routes
  */
  if (settings.serveWikiOnRoot === 'yes' ) {
    app.use('/', checkAuthentication, wiki.router)
  } else {
    app.use('/wiki', checkAuthentication, wiki.router)

    app.get('/js', function (req, res) {
      res.sendFile(__dirname + '/js/browser.js')
    })

    app.get('/settings', function (req, res) {
      res.send({'wssPort': settings.httpsPort})
    })

    app.get('/', function (req, res) {
      res.sendFile(__dirname + '/index.html')
    })
  }

  let server
  if (settings.useHTTPS) {
    // No try block here because it should crash if this fails.
    keypath = path.resolve(wiki.baseDir, settings.serverKeyPath)
    certpath = path.resolve(wiki.baseDir, settings.certPath)
    const options = {
      key: fs.readFileSync(keypath),
      cert: fs.readFileSync(certpath)
    }
    server = https.createServer(options, app).listen(settings.httpsPort)
    console.log(`HTTPS server on ${settings.httpsPort}`)
  } else {
    server = http.createServer(app).listen(settings.httpsPort)
    console.log(`HTTP server on ${settings.httpsPort}`)
  }
  wiki.tw.httpServerPort = settings.httpsPort

  // Create the websocket server
  const wsserver = require('./js/websocketserver.js')
  // Set the websocket server to use the https server
  wsserver.init(server)
  const messageHandlers = require('./js/websocketmessagehandlers.js')
  const adminHandlers = require('./js/websocketadminhandlers.js')
  messageHandlers.addHandlers(adminHandlers)
  messageHandlers.addHandlers(wiki.tw.nodeMessageHandlers)
  wiki.tw.connections = wsserver.connections
  wiki.tw.Bob.handleMessage = wsserver.handleMessage
}
