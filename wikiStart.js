const express = require('express')
const path = require('path')
const os = require('os')
const checkPermission = require('./js/checkPermissions.js')

let wiki = {}

module.exports = function(settings, baseDir) {
  wiki.settings = settings;
  wiki.baseDir = baseDir;
  /*
    This next block lets us set environment variables from the config files
    instead of the command line.
  */
  if (typeof settings.pluginsPath === 'string') {
    const resolvedpluginspath = path.resolve(wiki.baseDir,settings.pluginsPath);
    if (process.env["TIDDLYWIKI_PLUGIN_PATH"] !== undefined && process.env["TIDDLYWIKI_PLUGIN_PATH"] !== '') {
      process.env["TIDDLYWIKI_PLUGIN_PATH"] = process.env["TIDDLYWIKI_PLUGIN_PATH"] + path.delimiter + resolvedpluginspath;
    } else {
      process.env["TIDDLYWIKI_PLUGIN_PATH"] = resolvedpluginspath;
    }
  }
  if (typeof settings.themesPath === 'string') {
    const resolvedthemespath = path.resolve(wiki.baseDir,settings.themesPath);
    if (process.env["TIDDLYWIKI_THEME_PATH"] !== undefined && process.env["TIDDLYWIKI_THEME_PATH"] !== '') {
      process.env["TIDDLYWIKI_THEME_PATH"] = process.env["TIDDLYWIKI_THEME_PATH"] + path.delimiter + resolvedthemespath;
    } else {
      process.env["TIDDLYWIKI_THEME_PATH"] = resolvedthemespath;
    }
  }
  if (typeof settings.editionsPath === 'string') {
    const resolvededitionspath = path.resolve(wiki.baseDir,settings.editionsPath)
    if (process.env["TIDDLYWIKI_EDITION_PATH"] !== undefined && process.env["TIDDLYWIKI_EDITION_PATH"] !== '') {
      process.env["TIDDLYWIKI_EDITION_PATH"] = process.env["TIDDLYWIKI_EDITION_PATH"] + path.delimiter + resolvededitionspath;
    } else {
      process.env["TIDDLYWIKI_EDITION_PATH"] = resolvededitionspath;
    }
  }

  wiki.tw = require("./TiddlyWiki5/boot/boot.js").TiddlyWiki()

  const wikisPath = settings.wikisPath || 'Wikis'
  const rootWikiName = settings.rootWikiName || 'IndexWiki'

  const RootWikiPath = path.resolve(wiki.baseDir, rootWikiName)

  // Fake the command line arguments
  const args = [RootWikiPath, '--externalserver']
  wiki.tw.boot.argv = args

  wiki.tw.ExternalServer = wiki.tw.ExternalServer || {}

  // Boot the TW5 app
  wiki.tw.boot.boot()

  wiki.router = express.Router()

  wiki.router.use(function(request, response, next) {
    // Attach the wiki to the request on all requests so it is available to
    // handlers later.
    request.wiki = wiki
    next()
  })
  function importRoute(routeArray) {
    routeArray.forEach(function(routeData) {
      switch (routeData.verb) {
        case 'get':
          wiki.router.get(routeData.route, routeData.handler)
          break
        case 'post':
          wiki.router.post(routeData.route, routeData.handler)
          break
        default:
          break
      }
    })
  }

  const addRoutes = function () {
    // The loadWikisRoutes one has to go last because of how route precidence works.
    // There is probably a better way to do this.
    importRoute(require('./wikiRouter/credentialsAPIRoutes.js'))
    importRoute(require('./wikiRouter/loadFilesRoutes.js'))
    importRoute(require('./wikiRouter/pluginsAPIRoutes.js'))
    importRoute(require('./wikiRouter/pushAndFetchRoutes.js'))
    importRoute(require('./wikiRouter/uploadFileRoute.js'))
    importRoute(require('./wikiRouter/loadWikisRoutes.js'))
  }

  addRoutes()

  /*
    This lists a wiki in the wiki permissions
  */
  function listWiki(wikiName, currentLevel, data) {
    const nameParts = wikiName.split(path.sep)
    if (typeof currentLevel[nameParts[0]] === 'object' && nameParts.length > 1) {
      listWiki(nameParts.slice(1).join(path.sep), currentLevel[nameParts[0]], data)
    } else if (typeof currentLevel[nameParts[0]] === 'undefined' && nameParts.length > 1) {
      currentLevel[nameParts[0]] = {};
      listWiki(nameParts.slice(1).join(path.sep), currentLevel[nameParts[0]], data)
    } else if (nameParts.length === 1) {
      // For now assume that they mean what they say and overwrite anything
      // here if it exists.
      // List the wiki in the appropriate place
      currentLevel[nameParts[0]] = currentLevel[nameParts[0]] || {}
      currentLevel[nameParts[0]].__permissions = {}
      currentLevel[nameParts[0]].__permissions.public = data.public || 'no'
      currentLevel[nameParts[0]].__permissions.owner = data.decoded.name
    }
  }
  /*
    This goes through each wiki listed in the wiki permissions and checks if it
    exists. If it doesn't exist than the entry is removed.

    TODO - we are not using this yet because there is a strange edge case where
    it could remove a group that shouldn't be removed.
    If you make some wikis under foo (foo/bar, foo/baz etc.)
    then you make a wiki foo it replaces the entry in the wiki listing
    then you remove (or move) the wiki foo everything listed under foo gets
    removed from here.

    So two problems, one is you may be able to overwite a group listing with a
    single wiki, and two that could lead to losing access permission info.
  */
  function pruneWikiList(wikiPermissionsList, listedWikis) {
    // We assume that the wiki list in the settings has already been pruned.
    // or that it is in the desired form.
    wikiPermissionsList = wikiPermissionsList || {}
    listedWikis = listedWikis || {}
    Object.keys(wikiPermissionsList).forEach(function(name) {
      if (listedWikis[name] && typeof listedWikis[name] === 'string') {
        // It is a wiki with a path listed
        // I guess do nothing
      } else if (listedWikis[name] && typeof listedWikis[name] === 'object') {
        // It is a group, recurse
        pruneWikiList(wikiPermissionsList[name], listedWikis[name])
      } else if (!listedWikis[name] || listedWikis[name] === {}) {
        // it isn't listed so remove it.
        delete wikiPermissionsList[name]
      }
    })
  }
  /*
    This function initialises the access settings for a new wiki

    name is the name of the wiki actually used (it may be different than what
    is supplied in data due to naming conflicts)
    data is the message from the browser. data.name is the person who made the
    wiki.

    data can have permissions listed in it, but only up as high as the person
    has permissions.

    So if the person making the wiki has edit, view and admin access they
    couldn't give script access to anyone on the new wiki.

    owner = data.name
    public = data.public || 'no'
    editors = []
    viewers = []
    [wikis.(name).access]
      (optional access things here)
      Guest=["view"]

    All of these settings go into Local.toml
  */
  wiki.tw.ExternalServer = wiki.tw.ExternalServer || {}
  wiki.tw.ExternalServer.initialiseWikiSettings = function(name, data) {
    const wikiPermissionsObject = {wikis:{}};
    data.decoded = data.decoded || {};
    wikiPermissionsObject.wikis[name] = {
      __path: name,
      __permissions: {
        owner: data.decoded.name,
        public: data.public || "no",
        editors: [],
        viewer: [],
        fetchers: [],
        pushers: [],
        access: {
          Guest: [],
          Normal: [],
          Admin: []
        }
      }
    };
    require('./LoadConfig.js').saveSetting(wikiPermissionsObject).then(function(result) {
      console.log('initialise wiki setting',result)
    }).catch(function(err) {
      console.log(err)
    });
  }

  // Here these two functions are placeholders, they don't do anything here.
  // They are needed to make this work with the non-express server components.
  wiki.tw.httpServer = {}
  wiki.tw.httpServer.addOtherRoutes = function () {
    // Does nothing!
  }
  wiki.tw.httpServer.clearRoutes = function () {
    // Also does nothing!
  }

  // Set the access control function for Bob
  wiki.tw.Bob.AccessCheck = function(wikiName, token, action, accessType) {
    return checkPermission(wikiName, token, action, accessType)
  }
  return wiki
}

module.exports.wiki = wiki
